package ru.renessans.jvschool.volkov.task.manager.api.service.adapter;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.AbstractModelDTO;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;

public interface IAdapterService<D extends AbstractModelDTO, E extends AbstractModel> {

    @Nullable
    D toDTO(@Nullable E convertible);

    @Nullable
    E toModel(@Nullable D convertible);

}