package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.api.IService;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidValueException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidValuesException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;
import ru.renessans.jvschool.volkov.task.manager.repository.IRepository;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;

@Transactional
@RequiredArgsConstructor
public abstract class AbstractService<E extends AbstractModel> implements IService<E> {

    @NotNull
    private final IRepository<E> repository;

    @NotNull
    @SneakyThrows
    @Override
    public E save(@Nullable final E value) {
        if (Objects.isNull(value)) throw new InvalidValueException();
        return this.repository.save(value);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E getRecordById(@Nullable final String id) {
        if (StringUtils.isEmpty(id)) throw new InvalidUserIdException();
        return this.repository.getRecordById(id);
    }

    @SneakyThrows
    @Override
    public boolean existsRecordById(@Nullable final String id) {
        if (StringUtils.isEmpty(id)) throw new InvalidUserIdException();
        return this.repository.existsById(id);
    }

    @SneakyThrows
    @Override
    public int deleteRecordById(@Nullable final String id) {
        if (StringUtils.isEmpty(id)) throw new InvalidIdException();
        try {
            this.repository.deleteById(id);
        } catch (@NotNull final Exception exception) {
            return 0;
        }
        return 1;
    }

    @Nullable
    @SneakyThrows
    @Override
    public E deleteRecord(@Nullable final E value) {
        if (Objects.isNull(value)) throw new InvalidValueException();
        this.repository.delete(value);
        return value;
    }

    @Override
    public int deleteAllRecords() {
        @NotNull final Collection<E> values = this.exportRecords();
        if (values.size() == 0) return 0;
        values.forEach(this.repository::delete);
        return 1;
    }

    @NotNull
    @Transactional(readOnly = true)
    @Override
    public Collection<E> exportRecords() {
        return this.repository.findAll();
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<E> importRecords(@Nullable final Collection<E> values) {
        if (ValidRuleUtil.isNullOrEmpty(values)) throw new InvalidValuesException();
        return this.repository.saveAll(values);
    }

}