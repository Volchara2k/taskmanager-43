package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.model.Task;

public interface IUserTaskService extends IUserOwnerService<Task> {

    @NotNull
    Task addUserOwner(
            @Nullable Task task
    );

    @NotNull
    Task addUserOwner(
            @Nullable String userId,
            @Nullable String projectTitle,
            @Nullable String title,
            @Nullable String description
    );

}