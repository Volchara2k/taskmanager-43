package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.ISessionEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.SessionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
@Controller
@RequiredArgsConstructor
public final class SessionEndpoint implements ISessionEndpoint {

    @NotNull
    private final ISessionService sessionService;

    @NotNull
    private final ISessionAdapterService sessionAdapterService;


    @WebMethod
    @WebResult(name = "session", partName = "session")
    @Nullable
    @Override
    public SessionDTO validateSession(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @Nullable final Session validate = this.sessionService.validateSession(conversion);
        @Nullable final SessionDTO current = this.sessionAdapterService.toDTO(validate);
        return current;
    }

    @WebMethod
    @WebResult(name = "session", partName = "session")
    @Nullable
    @Override
    public SessionDTO validateSessionWithCommandRole(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "commandRole", partName = "commandRole") @Nullable final UserRoleType command
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @Nullable final Session current = this.sessionService.validateSession(conversion, command);
        @Nullable final SessionDTO validated = this.sessionAdapterService.toDTO(current);
        return validated;
    }

    @WebMethod
    @WebResult(name = "sessionValidState", partName = "sessionValidState")
    @NotNull
    @Override
    public SessionValidState verifyValidSessionState(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        return sessionService.verifyValidSessionState(conversion);
    }

    @WebMethod
    @WebResult(name = "permissionValidState", partName = "permissionValidState")
    @NotNull
    @Override
    public PermissionValidState verifyValidPermissionState(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "commandRoles", partName = "commandRoles") @Nullable final UserRoleType commandRole
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        return sessionService.verifyValidPermissionState(conversion, commandRole);
    }

}