package ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidIdException extends AbstractException {

    @NotNull
    private static final String EMPTY_ID = "Ошибка! Параметр \"идентификатор\" отсутствует!\n";

    public InvalidIdException() {
        super(EMPTY_ID);
    }

}