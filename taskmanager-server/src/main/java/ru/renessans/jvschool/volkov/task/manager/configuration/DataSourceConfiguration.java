package ru.renessans.jvschool.volkov.task.manager.configuration;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.api.configuration.IDataSourceConfiguration;
import ru.renessans.jvschool.volkov.task.manager.constant.DataSourceConst;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.config.InvalidDriverException;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@RequiredArgsConstructor
@EnableTransactionManagement
@ComponentScan("ru.renessans.jvschool.volkov.task.manager")
@PropertySource(value = "classpath:datasource-conf.properties")
@EnableJpaRepositories("ru.renessans.jvschool.volkov.task.manager.repository")
public class DataSourceConfiguration implements IDataSourceConfiguration {

    @NotNull
    private static final String MODELS_PACKAGE_SOURCE = "ru.renessans.jvschool.volkov.task.manager.model";

    @NotNull
    private final Environment environment;

    @Bean
    @SneakyThrows
    @NotNull
    @Override
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();

        @Nullable final String driver = this.environment.getProperty(DataSourceConst.DRIVER);
        if (StringUtils.isEmpty(driver)) throw new InvalidDriverException();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(this.environment.getProperty(DataSourceConst.URL));
        dataSource.setUsername(this.environment.getProperty(DataSourceConst.USER));
        dataSource.setPassword(this.environment.getProperty(DataSourceConst.PASS));

        return dataSource;
    }

    @Bean
    @NotNull
    @Override
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan(MODELS_PACKAGE_SOURCE);
        factoryBean.setJpaProperties(this.jpaProperties());
        return factoryBean;
    }

    @Bean
    @NotNull
    @Override
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @NotNull
    private Properties jpaProperties() {
        @NotNull final Properties properties = new Properties();
        properties.put(DataSourceConst.SHOW_SQL, this.environment.getProperty(DataSourceConst.SHOW_SQL));
        properties.put(DataSourceConst.FORMAT_SQL, this.environment.getProperty(DataSourceConst.FORMAT_SQL));
        properties.put(DataSourceConst.HBM2DDL_AUTO, this.environment.getProperty(DataSourceConst.HBM2DDL_AUTO));
        properties.put(DataSourceConst.DIALECT, this.environment.getProperty(DataSourceConst.DIALECT));
        properties.put(DataSourceConst.STATEMENT_BATCH_SIZE, this.environment.getProperty(DataSourceConst.STATEMENT_BATCH_SIZE));
        properties.put(DataSourceConst.BATCH_VERSIONED_DATA, this.environment.getProperty(DataSourceConst.BATCH_VERSIONED_DATA));
        properties.put(DataSourceConst.ORDER_UPDATES, this.environment.getProperty(DataSourceConst.ORDER_UPDATES));
        properties.put(DataSourceConst.ORDER_INSERTS, this.environment.getProperty(DataSourceConst.ORDER_INSERTS));
        return properties;
    }

}