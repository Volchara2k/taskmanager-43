package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.Cleanup;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.file.InvalidFileException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.file.InvalidPathnameException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidKeyException;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

@UtilityClass
public class DataSerializerUtil {

    @NotNull
    @SneakyThrows
    public <T extends Serializable> T writeToBin(
            @Nullable final T t,
            @Nullable final String pathname
    ) {
        if (Objects.isNull(t)) throw new InvalidKeyException();
        if (StringUtils.isEmpty(pathname)) throw new InvalidPathnameException();

        @NotNull final File file = FileUtil.create(pathname);
        @NotNull @Cleanup final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull @Cleanup final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(t);
        objectOutputStream.flush();

        return t;
    }

    @NotNull
    @SneakyThrows
    public <T extends Serializable> T readFromBin(
            @Nullable final String pathname,
            @Nullable final Class<T> tClass
    ) {
        if (StringUtils.isEmpty(pathname)) throw new InvalidPathnameException();
        if (Objects.isNull(tClass)) throw new InvalidKeyException();
        if (FileUtil.isNotExists(pathname)) throw new InvalidFileException();
        @NotNull @Cleanup final FileInputStream fileInputStream = new FileInputStream(pathname);
        @NotNull @Cleanup final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        return tClass.cast(objectInputStream.readObject());
    }

    @NotNull
    @SneakyThrows
    public <T extends Serializable> T writeToBase64(
            @Nullable final T t,
            @Nullable final String pathname
    ) {
        if (Objects.isNull(t)) throw new InvalidKeyException();
        if (StringUtils.isEmpty(pathname)) throw new InvalidPathnameException();

        @NotNull @Cleanup final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull @Cleanup final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(t);
        objectOutputStream.flush();

        final byte[] fileBytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(fileBytes);
        final byte[] base64Bytes = base64.getBytes(StandardCharsets.UTF_8);

        @NotNull final File file = FileUtil.create(pathname);
        @NotNull @Cleanup final FileOutputStream fileOutputStream = new FileOutputStream(file.getName());
        fileOutputStream.write(base64Bytes);
        fileOutputStream.flush();

        return t;
    }

    @NotNull
    @SneakyThrows
    public <T extends Serializable> T readFromBase64(
            @Nullable final String pathname,
            @Nullable final Class<T> tClass
    ) {
        if (StringUtils.isEmpty(pathname)) throw new InvalidPathnameException();
        if (Objects.isNull(tClass)) throw new InvalidKeyException();
        if (FileUtil.isNotExists(pathname)) throw new InvalidFileException();

        final byte[] fileBytes = FileUtil.read(pathname);
        @NotNull final String base64 = new String(fileBytes);
        final byte[] base64Bytes = new BASE64Decoder().decodeBuffer(base64);

        @NotNull @Cleanup final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(base64Bytes);
        @NotNull @Cleanup final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);

        return tClass.cast(objectInputStream.readObject());
    }

}