package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserDataValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.model.User;

public interface IAuthenticationService {

    @NotNull
    PermissionValidState verifyValidPermission(
            @Nullable String userId,
            @Nullable UserRoleType requiredRole
    );

    @NotNull
    UserDataValidState verifyValidUserData(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    User signUp(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    User signUp(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    User signUp(
            @Nullable String login,
            @Nullable String password,
            @Nullable UserRoleType role
    );

}