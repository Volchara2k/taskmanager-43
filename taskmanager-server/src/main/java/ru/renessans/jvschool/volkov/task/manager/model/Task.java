package ru.renessans.jvschool.volkov.task.manager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@Table(name = "tm_task")
public final class Task extends AbstractUserOwner {

    @Nullable
    @ManyToOne(fetch = FetchType.LAZY)
    private Project project;

    public Task(
            @NotNull final String title,
            @NotNull final String description
    ) {
        setTitle(title);
        setDescription(description);
    }

    public Task(
            @NotNull final String userId,
            @NotNull final String title,
            @NotNull final String description
    ) {
        setUserId(userId);
        setTitle(title);
        setDescription(description);
    }

    public Task(
            @NotNull final String id,
            @NotNull final String userId,
            @NotNull final String title,
            @NotNull final String description
    ) {
        setId(id);
        setUserId(userId);
        setTitle(title);
        setDescription(description);
    }

}