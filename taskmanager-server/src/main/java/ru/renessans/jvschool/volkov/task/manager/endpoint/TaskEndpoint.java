package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.ITaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserTaskService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;
import java.util.stream.Collectors;

@WebService
@Controller
@RequiredArgsConstructor
public final class TaskEndpoint implements ITaskEndpoint {

    @NotNull
    private final IUserTaskService taskUserService;

    @NotNull
    private final ITaskAdapterService taskAdapterService;

    @NotNull
    private final ISessionService sessionService;

    @NotNull
    private final ISessionAdapterService sessionAdapterService;

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @Override
    public TaskDTO addTaskForProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projectTitle", partName = "projectTitle") @Nullable final String projectTitle,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @NotNull final Task task = this.taskUserService.addUserOwner(current.getUserId(), projectTitle, title, description);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @Override
    public TaskDTO addTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @NotNull final Task task = this.taskUserService.addUserOwner(current.getUserId(), title, description);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "updatedTask", partName = "updatedTask")
    @Nullable
    @Override
    public TaskDTO updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Task task = this.taskUserService.updateUserOwnerById(current.getUserId(), id, title, description);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "updatedTask", partName = "updatedTask")
    @Nullable
    @Override
    public TaskDTO updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Task task = this.taskUserService.updateUserOwnerByIndex(current.getUserId(), index, title, description);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        return this.taskUserService.deleteUserOwnerById(current.getUserId(), id);
    }

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        return this.taskUserService.deleteUserOwnerByIndex(current.getUserId(), index);
    }

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteTaskByTitle(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "title", partName = "title") @Nullable final String title
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        return this.taskUserService.deleteUserOwnerByTitle(current.getUserId(), title);
    }

    @WebMethod
    @WebResult(name = "deletedTasks", partName = "deletedTasks")
    @Override
    public int deleteAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        return this.taskUserService.deleteUserOwnerAll(current.getUserId());
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @Override
    public TaskDTO getTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @Nullable final Task task = taskUserService.getUserOwnerById(current.getUserId(), id);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @Override
    public TaskDTO getTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Task task = this.taskUserService.getUserOwnerByIndex(current.getUserId(), index);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @Override
    public TaskDTO getTaskByTitle(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "title", partName = "title") @Nullable final String title
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Task task = this.taskUserService.getUserOwnerByTitle(current.getUserId(), title);
        return taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @NotNull
    @Override
    public Collection<TaskDTO> getAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        return this.taskUserService.getUserOwnerAll(current.getUserId())
                .stream()
                .map(this.taskAdapterService::toDTO)
                .collect(Collectors.toList());
    }

}