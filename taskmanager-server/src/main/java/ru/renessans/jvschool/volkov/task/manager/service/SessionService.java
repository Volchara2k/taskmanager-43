package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.SessionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserDataValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidLoginException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidPasswordException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.ISessionRepository;
import ru.renessans.jvschool.volkov.task.manager.util.SignatureUtil;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Objects;

@Service
@Transactional
public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IAuthenticationService authenticationService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IConfigurationService configurationService;

    public SessionService(
            @NotNull final ISessionRepository sessionRepository,
            @NotNull final IAuthenticationService authenticationService,
            @NotNull final IUserService userService,
            @NotNull final IConfigurationService configurationService
    ) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.authenticationService = authenticationService;
        this.userService = userService;
        this.configurationService = configurationService;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Session addSignature(
            @Nullable final Session session
    ) {
        if (Objects.isNull(session)) throw new AccessFailureException(SessionValidState.NO_SESSION.getTitle());
        session.setSignature(null);
        @NotNull final String salt = this.configurationService.getSessionSalt();
        @NotNull final Integer cycle = this.configurationService.getSessionCycle();
        @NotNull final String signature = SignatureUtil.getHashSignature(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Session openSession(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (StringUtils.isEmpty(login)) throw new InvalidLoginException();
        if (StringUtils.isEmpty(password)) throw new InvalidPasswordException();

        @NotNull final User user = validateUserData(login, password);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @NotNull final Session signatureSession = this.addSignature(session);

        return super.save(signatureSession);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Session closeSession(
            @Nullable final Session session
    ) {
        @Nullable final Session validate = this.validateSession(session);
        if (Objects.isNull(validate.getUserId())) throw new AccessFailureException(SessionValidState.NO_SESSION.getTitle());
        return super.deleteRecord(validate);
    }

    @Override
    public int closeAllSessions(
            @Nullable final Session session
    ) {
        this.validateSession(session);
        return super.deleteAllRecords();
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public Session getSessionByUserId(
            @Nullable final Session session
    ) {
        @NotNull final Session validate = this.validateSession(session);
        if (Objects.isNull(validate.getUserId())) throw new AccessFailureException(SessionValidState.NO_SESSION.getTitle());
        return this.sessionRepository.getSessionByUserId(validate.getUserId());
    }

    @SneakyThrows
    @Override
    public int closeSessionByUserId(
            @Nullable final Session session,
            @Nullable final String userId
    ) {
        @Nullable final Session validate = this.validateSession(session);
        if (Objects.isNull(userId)) throw new InvalidUserIdException();
        return this.sessionRepository.deleteSessionByUserId(validate.getId(), userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User validateUserData(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (StringUtils.isEmpty(login)) throw new InvalidLoginException();
        if (StringUtils.isEmpty(password)) throw new InvalidPasswordException();
        @NotNull final UserDataValidState authState = this.verifyValidUserData(login, password);
        if (authState.isNotSuccess()) throw new AccessFailureException(authState.getTitle());
        @Nullable final User user = this.userService.getUserByLogin(login);
        if (Objects.isNull(user)) throw new AccessFailureException(UserDataValidState.USER_NOT_FOUND.getTitle());
        return user;
    }

    @NotNull
    @SneakyThrows
    @Override
    public UserDataValidState verifyValidUserData(
            @Nullable final String login,
            @Nullable final String password
    ) {
        return this.authenticationService.verifyValidUserData(login, password);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Session validateSession(
            @Nullable final Session session
    ) {
        if (Objects.isNull(session)) throw new AccessFailureException(SessionValidState.NO_SESSION.getTitle());
        @NotNull final SessionValidState sessionState = this.verifyValidSessionState(session);
        if (sessionState.isNotSuccess()) throw new AccessFailureException(sessionState.getTitle());
        return session;
    }

    @Override
    @SneakyThrows
    @NotNull
    public Session validateSession(
            @Nullable final Session session,
            @Nullable final UserRoleType requiredRole
    ) {
        if (Objects.isNull(session)) throw new AccessFailureException(SessionValidState.NO_SESSION.getTitle());
        @NotNull final SessionValidState sessionState = this.verifyValidSessionState(session);
        if (sessionState.isNotSuccess()) throw new AccessFailureException(sessionState.getTitle());
        @NotNull final PermissionValidState permissionValidState = this.verifyValidPermissionState(session, requiredRole);
        if (permissionValidState.isNotSuccess()) throw new AccessFailureException(permissionValidState.getTitle());
        return session;
    }

    @NotNull
    @Transactional(readOnly = true)
    @Override
    public SessionValidState verifyValidSessionState(
            @Nullable final Session session
    ) {
        if (Objects.isNull(session)) return SessionValidState.NO_SESSION;
        if (StringUtils.isEmpty(session.getUserId())) return SessionValidState.NO_USER_ID;
        if (ValidRuleUtil.isNullOrEmpty(session.getTimestamp())) return SessionValidState.NO_TIMESTAMP;
        if (StringUtils.isEmpty(session.getSignature())) return SessionValidState.NO_SIGNATURE;

        @Nullable final Session temp = session.clone();
        if (Objects.isNull(temp)) return SessionValidState.NO_SESSION;

        @NotNull final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = this.addSignature(temp).getSignature();
        final boolean isEqualSignatures = signatureSource.equals(signatureTarget);
        if (!isEqualSignatures) return SessionValidState.DIFFERENT_SIGNATURES;
        if (!this.userService.existsRecordById(session.getUserId())) return SessionValidState.SESSION_CLOSED;

        return SessionValidState.SUCCESS;
    }

    @NotNull
    @Override
    public PermissionValidState verifyValidPermissionState(
            @Nullable final Session session,
            @Nullable final UserRoleType commandRole
    ) {
        if (Objects.isNull(session)) return PermissionValidState.NO_ACCESS_RIGHTS;
        return this.authenticationService.verifyValidPermission(session.getUserId(), commandRole);
    }

}