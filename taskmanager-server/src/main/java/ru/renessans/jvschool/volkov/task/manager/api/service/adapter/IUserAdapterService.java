package ru.renessans.jvschool.volkov.task.manager.api.service.adapter;

import ru.renessans.jvschool.volkov.task.manager.dto.AbstractModelDTO;
import ru.renessans.jvschool.volkov.task.manager.model.User;

public interface IUserAdapterService<D extends AbstractModelDTO> extends IAdapterService<D, User> {
}