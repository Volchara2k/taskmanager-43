package ru.renessans.jvschool.volkov.task.manager.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;

import java.util.Collection;

public interface IService<E extends AbstractModel> {

    @NotNull
    E save(@Nullable E value);

    @Nullable
    E getRecordById(@Nullable String id);

    boolean existsRecordById(@Nullable String id);

    int deleteRecordById(@NotNull String id);

    @Nullable
    E deleteRecord(@Nullable E value);

    int deleteAllRecords();

    @NotNull
    Collection<E> exportRecords();

    @NotNull
    Collection<E> importRecords(@Nullable Collection<E> values);

}