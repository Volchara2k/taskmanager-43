package ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidHostException extends AbstractException {

    @NotNull
    private static final String EMPTY_HOST = "Ошибка! Параметр \"host\" отсутствует!\n";

    public InvalidHostException() {
        super(EMPTY_HOST);
    }

}