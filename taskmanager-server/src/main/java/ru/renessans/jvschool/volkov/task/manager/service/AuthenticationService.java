package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserDataValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidFirstNameException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidLoginException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidPasswordException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserRoleException;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public final class AuthenticationService implements IAuthenticationService {

    @NotNull
    private final IUserService userService;

    @NotNull
    @SneakyThrows
    @Override
    public PermissionValidState verifyValidPermission(
            @Nullable final String userId,
            @Nullable final UserRoleType requiredRole
    ) {
        if (Objects.isNull(requiredRole)) return PermissionValidState.SUCCESS;
        @NotNull final UserRoleType userRoleType = this.userService.getUserRole(userId);

        final boolean isOnlyUnknownCommand = requiredRole.equals(UserRoleType.UNKNOWN);
        final boolean isAuthedUserRole = (userRoleType != UserRoleType.UNKNOWN);
        if (isOnlyUnknownCommand && isAuthedUserRole) return PermissionValidState.LOGGED;

        final boolean containsTypes = requiredRole.equals(userRoleType);
        if (containsTypes) return PermissionValidState.SUCCESS;

        final boolean isOnlyAdminCommand = requiredRole.equals(UserRoleType.ADMIN);
        final boolean isUserUserRole = (userRoleType == UserRoleType.USER);
        if (isOnlyAdminCommand && isUserUserRole) return PermissionValidState.NO_ACCESS_RIGHTS;

        return PermissionValidState.NEED_LOG_IN;
    }

    @NotNull
    @SneakyThrows
    @Override
    public UserDataValidState verifyValidUserData(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (StringUtils.isEmpty(login)) throw new InvalidLoginException();
        if (StringUtils.isEmpty(password)) throw new InvalidPasswordException();

        @Nullable final User user = this.userService.getUserByLogin(login);
        if (Objects.isNull(user)) return UserDataValidState.USER_NOT_FOUND;
        if (user.getLockdown()) return UserDataValidState.LOCKDOWN_PROFILE;

        @NotNull final String passwordHash = HashUtil.getSaltHashLine(password);
        final boolean isNotEqualPasswords = !passwordHash.equals(user.getPasswordHash());
        if (isNotEqualPasswords) return UserDataValidState.INVALID_PASSWORD;

        return UserDataValidState.SUCCESS;
    }

    @NotNull
    @SneakyThrows
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (StringUtils.isEmpty(login)) throw new InvalidLoginException();
        if (StringUtils.isEmpty(password)) throw new InvalidPasswordException();
        return this.userService.addUser(login, password);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String firstName
    ) {
        if (StringUtils.isEmpty(login)) throw new InvalidLoginException();
        if (StringUtils.isEmpty(password)) throw new InvalidPasswordException();
        if (StringUtils.isEmpty(firstName)) throw new InvalidFirstNameException();
        return this.userService.addUser(login, password, firstName);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final UserRoleType role
    ) {
        if (StringUtils.isEmpty(login)) throw new InvalidLoginException();
        if (StringUtils.isEmpty(password)) throw new InvalidPasswordException();
        if (Objects.isNull(role)) throw new InvalidUserRoleException();
        return this.userService.addUser(login, password, role);
    }

}