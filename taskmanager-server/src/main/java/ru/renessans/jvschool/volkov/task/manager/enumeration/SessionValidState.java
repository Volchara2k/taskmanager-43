package ru.renessans.jvschool.volkov.task.manager.enumeration;

import org.jetbrains.annotations.NotNull;

public enum SessionValidState {

    SUCCESS("Успешно!"),

    NO_SESSION("Сессии пользователя не существует. Необходимо авторизоваться!"),

    NO_USER_ID("Нет идентификатора пользователя"),

    NO_TIMESTAMP("Нет времени подключения к сессии!"),

    NO_SIGNATURE("Нет подписи!"),

    DIFFERENT_SIGNATURES("Подписи различаются!"),

    SESSION_CLOSED("Сессия была закрыта принудительно!");

    @NotNull
    private final String title;

    SessionValidState(@NotNull final String title) {
        this.title = title;
    }

    @NotNull
    public String getTitle() {
        return title;
    }

    public boolean isNotSuccess() {
        return this != SUCCESS;
    }

}