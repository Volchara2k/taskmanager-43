package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IService;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.SessionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserDataValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;

public interface ISessionService extends IService<Session> {

    @NotNull
    Session addSignature(
            @Nullable Session session
    );

    @NotNull
    Session openSession(
            @Nullable String login,
            @Nullable String password
    );

    @Nullable
    Session closeSession(
            @Nullable Session session
    );

    int closeAllSessions(
            @Nullable Session session
    );

    @Nullable
    Session getSessionByUserId(
            @Nullable Session session
    );

    int closeSessionByUserId(
            @Nullable Session session,
            @Nullable String userId
    );

    @NotNull
    User validateUserData(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    UserDataValidState verifyValidUserData(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    Session validateSession(
            @Nullable Session session
    );

    @NotNull
    Session validateSession(
            @Nullable Session session,
            @Nullable UserRoleType requiredRole
    );

    @NotNull
    SessionValidState verifyValidSessionState(
            @Nullable Session session
    );

    @NotNull
    PermissionValidState verifyValidPermissionState(
            @Nullable Session session,
            @Nullable UserRoleType commandRole
    );

}