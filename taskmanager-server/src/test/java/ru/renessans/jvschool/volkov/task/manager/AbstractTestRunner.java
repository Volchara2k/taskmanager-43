package ru.renessans.jvschool.volkov.task.manager;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.runner.AbstractRepositoryTestRunner;
import ru.renessans.jvschool.volkov.task.manager.runner.AbstractServiceTestRunner;
import ru.renessans.jvschool.volkov.task.manager.runner.AbstractUtilityTestRunner;

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                AbstractRepositoryTestRunner.class,
                AbstractServiceTestRunner.class,
                AbstractUtilityTestRunner.class
        }
)

public abstract class AbstractTestRunner {
}