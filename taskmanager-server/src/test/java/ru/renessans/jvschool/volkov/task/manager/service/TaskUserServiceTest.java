package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataBaseProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataTaskProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Collection;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(value = JUnitParamsRunner.class)
public final class TaskUserServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConfigurationService CONFIG_SERVICE = new ConfigurationService(PROPERTY_SERVICE);

    @NotNull
    private static final IUserService USER_SERVICE = new UserService(null);

    private static final IUserProjectService PROJECT_USER_REPOSITORY = new UserProjectService(null, null);

    @NotNull
    private static final IUserTaskService TASK_SERVICE = new UserTaskService(null, null, PROJECT_USER_REPOSITORY);

    @BeforeClass
    public static void preparingConfigurationBefore() {
        Assert.assertNotNull(PROPERTY_SERVICE);
        Assert.assertNotNull(CONFIG_SERVICE);
        PROPERTY_SERVICE.load();
    }

    @BeforeClass
    public static void assertMainComponentsNotNullBefore() {
        Assert.assertNotNull(USER_SERVICE);
        Assert.assertNotNull(TASK_SERVICE);
    }

    @Test
    @TestCaseName("Run testNegativeAdd for add(\"{0}\", \"{1}\", \"{2}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidOwnerMainFieldsCaseData"
    )
    public void testNegativeAdd(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        Assert.assertNotNull(TASK_SERVICE);

        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> TASK_SERVICE.addUserOwner(userId, title, description)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final InvalidTitleException titleThrown = assertThrows(
                InvalidTitleException.class,
                () -> TASK_SERVICE.addUserOwner(userIdTemp, title, description)
        );
        Assert.assertNotNull(titleThrown);
        Assert.assertNotNull(titleThrown.getMessage());

        @NotNull final String titleTemp = DemoDataConst.DEMO_TITLE_1ST;
        @NotNull final InvalidDescriptionException descriptionThrown = assertThrows(
                InvalidDescriptionException.class,
                () -> TASK_SERVICE.addUserOwner(userIdTemp, titleTemp, description)
        );
        Assert.assertNotNull(descriptionThrown);
        Assert.assertNotNull(descriptionThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeUpdateByIndex for updateByIndex(\"{0}\", {1}, \"{2}\", \"{3}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidOwnerWithIndexCaseData"
    )
    public void testNegativeUpdateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String title,
            @Nullable final String description
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> TASK_SERVICE.updateUserOwnerByIndex(userId, index, title, description)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final IllegalIndexException indexThrown = assertThrows(
                IllegalIndexException.class,
                () -> TASK_SERVICE.updateUserOwnerByIndex(userIdTemp, index, title, description)
        );
        Assert.assertNotNull(indexThrown);
        Assert.assertNotNull(indexThrown.getMessage());

        @NotNull final Integer indexTemp = 0;
        Assert.assertNotNull(indexTemp);
        @NotNull final InvalidTitleException titleThrown = assertThrows(
                InvalidTitleException.class,
                () -> TASK_SERVICE.updateUserOwnerByIndex(userIdTemp, indexTemp, title, description)
        );
        Assert.assertNotNull(titleThrown);
        Assert.assertNotNull(titleThrown.getMessage());

        @NotNull final String titleTemp = DemoDataConst.DEMO_TITLE_1ST;
        @NotNull final InvalidDescriptionException descriptionThrown = assertThrows(
                InvalidDescriptionException.class,
                () -> TASK_SERVICE.addUserOwner(userIdTemp, titleTemp, description)
        );
        Assert.assertNotNull(descriptionThrown);
        Assert.assertNotNull(descriptionThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeUpdateById for updateById(\"{0}\", \"{1}\", \"{2}\", \"{3}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidOwnerWithIdCaseData"
    )
    public void testNegativeUpdateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String title,
            @Nullable final String description
    ) {

        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> TASK_SERVICE.updateUserOwnerById(userId, id, title, description)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final InvalidIdException indexThrown = assertThrows(
                InvalidIdException.class,
                () -> TASK_SERVICE.updateUserOwnerById(userIdTemp, id, title, description)
        );
        Assert.assertNotNull(indexThrown);
        Assert.assertNotNull(indexThrown.getMessage());

        @NotNull final String idTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(idTemp);
        @NotNull final InvalidTitleException titleThrown = assertThrows(
                InvalidTitleException.class,
                () -> TASK_SERVICE.updateUserOwnerById(userIdTemp, idTemp, title, description)
        );
        Assert.assertNotNull(titleThrown);
        Assert.assertNotNull(titleThrown.getMessage());

        @NotNull final String titleTemp = DemoDataConst.DEMO_TITLE_1ST;
        @NotNull final InvalidDescriptionException descriptionThrown = assertThrows(
                InvalidDescriptionException.class,
                () -> TASK_SERVICE.addUserOwner(userIdTemp, titleTemp, description)
        );
        Assert.assertNotNull(descriptionThrown);
        Assert.assertNotNull(descriptionThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeDeleteByIndex for deleteByIndex(\"{0}\", {1})")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidOwnerIndexCaseData"
    )
    public void testNegativeDeleteByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> TASK_SERVICE.deleteUserOwnerByIndex(userId, index)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final IllegalIndexException indexThrown = assertThrows(
                IllegalIndexException.class,
                () -> TASK_SERVICE.deleteUserOwnerByIndex(userIdTemp, index)
        );
        Assert.assertNotNull(indexThrown);
        Assert.assertNotNull(indexThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeDeleteById for deleteById(\"{0}\", \"{1}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersEditableCaseData"
    )
    public void testNegativeDeleteById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> TASK_SERVICE.deleteUserOwnerById(userId, id)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final InvalidIdException idThrown = assertThrows(
                InvalidIdException.class,
                () -> TASK_SERVICE.deleteUserOwnerById(userIdTemp, id)
        );
        Assert.assertNotNull(idThrown);
        Assert.assertNotNull(idThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeDeleteByTitle for deleteByTitle(\"{0}\", \"{1}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersEditableCaseData"
    )
    public void testNegativeDeleteByTitle(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> TASK_SERVICE.deleteUserOwnerById(userId, id)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final InvalidTitleException titleThrown = assertThrows(
                InvalidTitleException.class,
                () -> TASK_SERVICE.deleteUserOwnerByTitle(userIdTemp, id)
        );
        Assert.assertNotNull(titleThrown);
        Assert.assertNotNull(titleThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeDeleteAll for deleteAll(\"{0}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeDeleteAll(
            @Nullable final String userId
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> TASK_SERVICE.deleteUserOwnerAll(userId)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeGetByIndex for getByIndex(\"{0}\", {1})")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidOwnerIndexCaseData"
    )
    public void testNegativeGetByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> TASK_SERVICE.getUserOwnerByIndex(userId, index)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final IllegalIndexException indexThrown = assertThrows(
                IllegalIndexException.class,
                () -> TASK_SERVICE.getUserOwnerByIndex(userIdTemp, index)
        );
        Assert.assertNotNull(indexThrown);
        Assert.assertNotNull(indexThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeGetById for getById(\"{0}\", \"{1}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersEditableCaseData"
    )
    public void testNegativeGetById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> TASK_SERVICE.getUserOwnerById(userId, id)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final InvalidIdException idThrown = assertThrows(
                InvalidIdException.class,
                () -> TASK_SERVICE.getUserOwnerById(userIdTemp, id)
        );
        Assert.assertNotNull(idThrown);
        Assert.assertNotNull(idThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeGetByTitle for getByTitle(\"{0}\", \"{1}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersEditableCaseData"
    )
    public void testNegativeGetByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> TASK_SERVICE.getUserOwnerByTitle(userId, title)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final InvalidTitleException titleThrown = assertThrows(
                InvalidTitleException.class,
                () -> TASK_SERVICE.getUserOwnerByTitle(userIdTemp, title)
        );
        Assert.assertNotNull(titleThrown);
        Assert.assertNotNull(titleThrown.getMessage());
    }

    @Test(expected = InvalidUserIdException.class)
    @TestCaseName("Run testNegativeGetAll for getAll(\"{0}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeGetAll(
            @Nullable final String userId
    ) {
        TASK_SERVICE.getUserOwnerAll(userId);
    }

    @Test(expected = InvalidUserException.class)
    @TestCaseName("Run testInitialDemoData for initialDemoData({0})")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidCollectionsUsersCaseData"
    )
    public void testNegativeInitialDemoData(
            @Nullable final Collection<User> users
    ) {
        TASK_SERVICE.initialUserOwner(users);
    }

    @Test
    @TestCaseName("Run testAdd for add({0}, {1})")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testAdd(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);

        @NotNull final Task addTask = TASK_SERVICE.addUserOwner(user.getId(), task.getTitle(), task.getDescription());
        Assert.assertNotNull(addTask);
        Assert.assertEquals(task.getUserId(), addTask.getUserId());
        Assert.assertEquals(task.getTitle(), addTask.getTitle());
        Assert.assertEquals(task.getDescription(), addTask.getDescription());
        @Nullable final Task cleared = TASK_SERVICE.deleteRecord(addTask);
        Assert.assertNotNull(cleared);
    }

//    @Test
//    @TestCaseName("Run testUpdateByIndex for updateByIndex({0}, 0, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testUpdateByIndex(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Task addTask = TASK_SERVICE.add(addUser.getId(), task.getTitle(), task.getDescription());
//        Assert.assertNotNull(addTask);
//
//        @NotNull final String newData = UUID.randomUUID().toString();
//        Assert.assertNotNull(newData);
//        @Nullable final Task updateTask = TASK_SERVICE.updateByIndex(addUser.getId(), 0, newData, newData);
//        Assert.assertNotNull(updateTask);
//        Assert.assertEquals(addTask.getId(), updateTask.getId());
//        Assert.assertEquals(addTask.getUserId(), updateTask.getUserId());
//        Assert.assertEquals(newData, updateTask.getTitle());
//        Assert.assertEquals(newData, updateTask.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testUpdateById for updateById({0}, {1}")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testUpdateById(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Task addTask = TASK_SERVICE.add(addUser.getId(), task.getTitle(), task.getDescription());
//        Assert.assertNotNull(addTask);
//
//        @NotNull final String newData = UUID.randomUUID().toString();
//        Assert.assertNotNull(newData);
//        @Nullable final Task updateTask = TASK_SERVICE.updateById(addUser.getId(), addTask.getId(), newData, newData);
//        Assert.assertNotNull(updateTask);
//        Assert.assertEquals(addTask.getId(), updateTask.getId());
//        Assert.assertEquals(addTask.getUserId(), updateTask.getUserId());
//        Assert.assertEquals(newData, updateTask.getTitle());
//        Assert.assertEquals(newData, updateTask.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteByIndex for deleteByIndex({0}, 0)")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testDeleteByIndex(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Task addTask = TASK_SERVICE.add(addUser.getId(), task.getTitle(), task.getDescription());
//        Assert.assertNotNull(addTask);
//
//        @Nullable final Task deleteTask = TASK_SERVICE.deleteByIndex(addUser.getId(), 0);
//        Assert.assertNotNull(deleteTask);
//        Assert.assertEquals(addTask.getId(), deleteTask.getId());
//        Assert.assertEquals(addTask.getUserId(), deleteTask.getUserId());
//        Assert.assertEquals(addTask.getTitle(), deleteTask.getTitle());
//        Assert.assertEquals(addTask.getDescription(), deleteTask.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteById for deleteById({0}, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testDeleteById(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Task addTask = TASK_SERVICE.add(addUser.getId(), task.getTitle(), task.getDescription());
//        Assert.assertNotNull(addTask);
//
//        @Nullable final Task deleteTask = TASK_SERVICE.deleteById(addUser.getId(), addTask.getId());
//        Assert.assertNotNull(deleteTask);
//        Assert.assertEquals(addTask.getId(), deleteTask.getId());
//        Assert.assertEquals(addTask.getUserId(), deleteTask.getUserId());
//        Assert.assertEquals(addTask.getTitle(), deleteTask.getTitle());
//        Assert.assertEquals(addTask.getDescription(), deleteTask.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteByTitle for deleteByTitle({0}, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testDeleteByTitle(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Task addTask = TASK_SERVICE.add(addUser.getId(), task.getTitle(), task.getDescription());
//        Assert.assertNotNull(addTask);
//
//        @Nullable final Task deleteTask = TASK_SERVICE.deleteByTitle(addUser.getId(), addTask.getTitle());
//        Assert.assertNotNull(deleteTask);
//        Assert.assertEquals(addTask.getId(), deleteTask.getId());
//        Assert.assertEquals(addTask.getUserId(), deleteTask.getUserId());
//        Assert.assertEquals(addTask.getTitle(), deleteTask.getTitle());
//        Assert.assertEquals(addTask.getDescription(), deleteTask.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteAll for deleteAll({0})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testDeleteAll(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Task addTask = TASK_SERVICE.add(addUser.getId(), task.getTitle(), task.getDescription());
//        Assert.assertNotNull(addTask);
//
//        @Nullable final Collection<Task> deleteTasks = TASK_SERVICE.deleteAll(addUser.getId());
//        Assert.assertNotNull(deleteTasks);
//        Assert.assertNotEquals(0, deleteTasks.size());
//        final boolean isUserTasks = deleteTasks.stream().allMatch(entity -> addUser.getId().equals(entity.getUserId()));
//        Assert.assertTrue(isUserTasks);
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testGetByIndex for getByIndex({0}, 0)")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testGetByIndex(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Task addTask = TASK_SERVICE.add(addUser.getId(), task.getTitle(), task.getDescription());
//        Assert.assertNotNull(addTask);
//
//        @Nullable final Task getTask = TASK_SERVICE.getByIndex(addUser.getId(), 0);
//        Assert.assertNotNull(getTask);
//        Assert.assertEquals(addTask.getId(), getTask.getId());
//        Assert.assertEquals(addTask.getUserId(), getTask.getUserId());
//        Assert.assertEquals(addTask.getTitle(), getTask.getTitle());
//        Assert.assertEquals(addTask.getDescription(), getTask.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testGetById for getById({0}, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testGetById(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Task addTask = TASK_SERVICE.add(addUser.getId(), task.getTitle(), task.getDescription());
//        Assert.assertNotNull(addTask);
//
//        @Nullable final Task getTask = TASK_SERVICE.getById(addUser.getId(), addTask.getId());
//        Assert.assertNotNull(getTask);
//        Assert.assertEquals(addTask.getId(), getTask.getId());
//        Assert.assertEquals(addTask.getUserId(), getTask.getUserId());
//        Assert.assertEquals(addTask.getTitle(), getTask.getTitle());
//        Assert.assertEquals(addTask.getDescription(), getTask.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testGetByTitle for getByTitle({0}, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testGetByTitle(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Task addTask = TASK_SERVICE.add(addUser.getId(), task.getTitle(), task.getDescription());
//        Assert.assertNotNull(addTask);
//
//        @Nullable final Task getTask = TASK_SERVICE.getByTitle(addUser.getId(), addTask.getTitle());
//        Assert.assertNotNull(getTask);
//        Assert.assertEquals(addTask.getId(), getTask.getId());
//        Assert.assertEquals(addTask.getUserId(), getTask.getUserId());
//        Assert.assertEquals(addTask.getTitle(), getTask.getTitle());
//        Assert.assertEquals(addTask.getDescription(), getTask.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testGetAll for getAll({0})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testGetAll(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Task addTask = TASK_SERVICE.add(addUser.getId(), task.getTitle(), task.getDescription());
//        Assert.assertNotNull(addTask);
//
//        @Nullable final Collection<Task> getTasks = TASK_SERVICE.getAll(addUser.getId());
//        Assert.assertNotNull(getTasks);
//        Assert.assertNotEquals(0, getTasks.size());
//        final boolean isUserTasks = getTasks.stream().allMatch(entity -> addUser.getId().equals(entity.getUserId()));
//        Assert.assertTrue(isUserTasks);
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }

    @Test
    @TestCaseName("Run testInitialDemoData for initialDemoData()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validCollectionUsersCaseData"
    )
    public void testInitialDemoData(
            @NotNull final Collection<User> users
    ) {
        Assert.assertNotNull(users);

        @NotNull final Collection<Task> initTasks = TASK_SERVICE.initialUserOwner(users);
        Assert.assertNotNull(initTasks);
        Assert.assertNotEquals(0, initTasks.size());
        initTasks.forEach(task -> {
            @Nullable final Task cleared = TASK_SERVICE.deleteRecord(task);
            Assert.assertNotNull(cleared);
        });
    }

}