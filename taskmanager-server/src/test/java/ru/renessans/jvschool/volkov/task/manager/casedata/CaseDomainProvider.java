package ru.renessans.jvschool.volkov.task.manager.casedata;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserUnlimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;

import java.util.Collections;
import java.util.UUID;

public final class CaseDomainProvider {

    @SuppressWarnings("unused")
    public Object[] validCollectionDomainsCaseData() {
        @NotNull final UserUnlimitedDTO userTestCase1st = UserUnlimitedDTO.builder()
                .id("2IHVObCtEbD15Rx9S7TP")
                .login(UUID.randomUUID().toString())
                .passwordHash(UUID.randomUUID().toString())
                .role(UserRoleType.USER)
                .lockdown(false)
                .build();
        @NotNull final ProjectDTO projectTestCase1st = ProjectDTO.builder()
                .id("Tf572BIqHenIMXwrZYNu")
                .userId(userTestCase1st.getId())
                .title(UUID.randomUUID().toString())
                .description(UUID.randomUUID().toString())
                .build();
        @NotNull final TaskDTO taskTestCase1st = TaskDTO.builder()
                .id("0htY0TSJMDryJnrIQ89q")
                .userId(userTestCase1st.getId())
                .title(UUID.randomUUID().toString())
                .description(UUID.randomUUID().toString())
                .build();
        @NotNull final DomainDTO domainTestCase1st = DomainDTO.builder()
                .projects(
                        Collections.singletonList(projectTestCase1st)
                )
                .tasks(
                        Collections.singletonList(taskTestCase1st)
                )
                .users(
                        Collections.singletonList(userTestCase1st)
                )
                .build();

        @NotNull final UserUnlimitedDTO userTestCase2st = UserUnlimitedDTO.builder()
                .id("WUnp3XFAp8dNnuCBFEAI")
                .login(UUID.randomUUID().toString())
                .passwordHash(UUID.randomUUID().toString())
                .role(UserRoleType.USER)
                .lockdown(false)
                .build();
        @NotNull final ProjectDTO projectTestCase2st = ProjectDTO.builder()
                .id("JJ48SIbvkeK2YYoYOUp6")
                .userId(userTestCase1st.getId())
                .title(UUID.randomUUID().toString())
                .description(UUID.randomUUID().toString())
                .build();
        @NotNull final TaskDTO taskTestCase2st = TaskDTO.builder()
                .id("PvJ6SewnJZWlBVdlthHG")
                .userId(userTestCase1st.getId())
                .title(UUID.randomUUID().toString())
                .description(UUID.randomUUID().toString())
                .build();
        @NotNull final DomainDTO domainTestCase2st = DomainDTO.builder()
                .projects(
                        Collections.singletonList(projectTestCase1st)
                )
                .tasks(
                        Collections.singletonList(taskTestCase1st)
                )
                .users(
                        Collections.singletonList(userTestCase1st)
                )
                .build();

        @NotNull final DomainDTO domainTestCase3rd = new DomainDTO();
        @NotNull final ProjectDTO projectTestCase3rd = new ProjectDTO();
        projectTestCase1st.setTitle(DemoDataConst.DEMO_TITLE_2ND);
        projectTestCase1st.setDescription(DemoDataConst.DEMO_DESCRIPTION_2ND);
        @NotNull final TaskDTO taskTestCase3rd = new TaskDTO();
        taskTestCase1st.setTitle(DemoDataConst.DEMO_TITLE_1ST);
        taskTestCase1st.setDescription(DemoDataConst.DEMO_DESCRIPTION_1ST);
        @NotNull final UserUnlimitedDTO userTestCase3rd = new UserUnlimitedDTO();
        userTestCase1st.setLogin("nnerVw24PV");
        userTestCase1st.setPasswordHash("nnerVw24PV");
        domainTestCase3rd.setProjects(
                Collections.singletonList(
                        projectTestCase3rd
                )
        );
        domainTestCase3rd.setTasks(
                Collections.singletonList(
                        taskTestCase3rd

                )
        );
        domainTestCase3rd.setUsers(
                Collections.singletonList(
                        userTestCase3rd
                )
        );

        return new Object[]{
                new Object[]{
                        domainTestCase1st
                },
                new Object[]{
                        domainTestCase2st
                }
        };
    }

}