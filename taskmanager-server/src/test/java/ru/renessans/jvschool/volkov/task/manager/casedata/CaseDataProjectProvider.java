package ru.renessans.jvschool.volkov.task.manager.casedata;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.User;

public final class CaseDataProjectProvider {

    @SuppressWarnings("unused")
    public Object[] validProjectsCaseData() {
        @Nullable User user;
        return new Object[]{
                new Object[]{
                        user = new User(
                                "eZ7jautHKpMedsGGZjlN",
                                "LIH5rdyt0B95tXfujB8U"
                        ),
                        new Project(
                                user.getId(),
                                "TH9X0zwSJHwni4lCaKV9",
                                "ePZAE5VOzQBjos1hSx8u"
                        )
                },
                new Object[]{
                        user = new User(
                                "S9UwYwXLYr0mItuR62RT",
                                "JuRjPc9MaLyekq2wlUaI"
                        ),
                        new Project(
                                user.getId(),
                                "6GN4YiYEys13PThxXb10",
                                "My3ZR4o6SpFPYlNOtClS"
                        )
                },
                new Object[]{
                        user = new User(
                                "sPcoZWNQSd5MdNh2xets",
                                "fDp35xmsrKoLsq0kfG70",
                                UserRoleType.USER
                        ),
                        new Project(
                                user.getId(),
                                "M2W3rPizobp07UGwzlrg",
                                "mD5R1Sj0U5cz6ZxoGFOV"
                        )
                }
        };
    }

}