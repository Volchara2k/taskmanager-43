package ru.renessans.jvschool.volkov.task.manager.casedata;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;

public final class CaseDataSessionProvider {

    @SuppressWarnings("unused")
    public Object[] validSessionsCaseData() {
        @Nullable User user;
        return new Object[]{
                new Object[]{
                        user = new User(
                                "sB3qt6uuidJ5a9m2AXdu",
                                "ckGSUuAh6m7nN7G1Y0u2"
                        ),
                        new Session(
                                user.getId(),
                                System.currentTimeMillis()
                        )
                },
                new Object[]{
                        user = new User(
                                "EeRkaojZhGYgCUfs2gL1",
                                "mpeCB7FblQFQVn0YEaON"
                        ),
                        new Session(
                                user.getId(),
                                System.currentTimeMillis()
                        )
                },
                new Object[]{
                        user = new User(
                                "4441ZlaqbdNeqkXaHpkE",
                                "CFP0bDpmQtJtbzbgMyla",
                                UserRoleType.ADMIN
                        ),
                        new Session(
                                user.getId(),
                                System.currentTimeMillis()
                        )
                }
        };
    }

}