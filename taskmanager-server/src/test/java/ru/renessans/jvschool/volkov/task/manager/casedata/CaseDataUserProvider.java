package ru.renessans.jvschool.volkov.task.manager.casedata;

import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public final class CaseDataUserProvider {

    @SuppressWarnings("unused")
    public Object[] validUsersCaseData() {
        return new Object[]{
                new Object[]{
                        new User(
                                "DemoDataConst.USER_TEST_LOGIN",
                                "DemoDataConst.USER_TEST_PASSWORD"
                        )
                },
                new Object[]{
                        new User(
                                "HashUtil.getSaltHashLine(DemoDataConst.USER_DEFAULT_LOGIN)",
                                "HashUtil.getSaltHashLine(DemoDataConst.USER_DEFAULT_PASSWORD)",
                                "DemoDataConst.USER_DEFAULT_FIRSTNAME"
                        )
                },
                new Object[]{
                        new User(
                                "HashUtil.getHashLine(DemoDataConst.USER_ADMIN_LOGIN)",
                                "HashUtil.getHashLine(DemoDataConst.USER_ADMIN_PASSWORD)",
                                UserRoleType.ADMIN
                        )
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] validCollectionUsersCaseData() {
        return new Object[]{
                new Object[]{
                        Collections.singletonList(
                                new User(
                                        "MWGhX8WydHzBpAFDfqTc",
                                        "WrL5uBCGS06wOBGCwt7z"
                                )
                        )
                },
                new Object[]{
                        Collections.singletonList(
                                new User(
                                        "8s7iVBN9Sz4HRIDca5IU",
                                        "YFjD1Q2wb9Y6kUd2oT01",
                                        "jXG9va5y2ElWcvscdqBf"
                                )
                        )
                },
                new Object[]{
                        Collections.singletonList(
                                new User(
                                        "PYYZoSueLkPu99qYrWUJ",
                                        "mRX60TaQJCMptDgJmDO0",
                                        UserRoleType.ADMIN
                                )
                        )
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] validUsersMainFieldsCaseData() {
        return new Object[]{
                new Object[]{
                        "eytqydJWHHrSc3zAJGiO",
                        "rMhXR748353dwtuWx5J4"
                },
                new Object[]{
                        "D9RG0h1CNih3VpVFlUzi",
                        "RzmgTJ7iFDfbwZKn4KQM"
                },
                new Object[]{
                        "T0KGfyerjEC76P1No5WW",
                        "7gmsLLf1WMOaP7PTt4Jv"
                },
                new Object[]{
                        ".....",
                        "!23"
                },
                new Object[]{
                        "dwa       dwa",
                        "1      "
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] validUsersMainFieldsWithNewEntityCaseData() {
        return new Object[]{
                new Object[]{
                        DemoDataConst.USER_TEST_LOGIN,
                        DemoDataConst.USER_TEST_PASSWORD,
                        "fg6A0Induv"
                },
                new Object[]{
                        HashUtil.getSaltHashLine(DemoDataConst.USER_DEFAULT_LOGIN),
                        HashUtil.getSaltHashLine(DemoDataConst.USER_DEFAULT_PASSWORD),
                        "JyWYAYmqJf"
                },
                new Object[]{
                        HashUtil.getHashLine(DemoDataConst.USER_ADMIN_LOGIN),
                        HashUtil.getHashLine(DemoDataConst.USER_ADMIN_PASSWORD),
                        "nnerVw24PV"
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] validUsersMainFieldsWithRoleCaseData() {
        return new Object[]{
                new Object[]{
                        "eytqydJWHHrSc3zAJGiO",
                        "rMhXR748353dwtuWx5J4",
                        UserRoleType.USER
                },
                new Object[]{
                        "D9RG0h1CNih3VpVFlUzi",
                        "RzmgTJ7iFDfbwZKn4KQM",
                        UserRoleType.USER
                },
                new Object[]{
                        "T0KGfyerjEC76P1No5WW",
                        "7gmsLLf1WMOaP7PTt4Jv",
                        UserRoleType.USER
                },
                new Object[]{
                        ".....",
                        "!23",
                        UserRoleType.USER
                },
                new Object[]{
                        "dwa       dwa",
                        "1      ",
                        UserRoleType.USER
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] invalidUsersAllFieldsCaseData() {
        return new Object[]{
                new Object[]{
                        null,
                        null,
                        null,
                        null
                },
                new Object[]{
                        "",
                        "",
                        "",
                        null
                },
                new Object[]{
                        "   ",
                        "   ",
                        "   ",
                        null
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] invalidUsersEditableCaseData() {
        return new Object[]{
                new Object[]{
                        null,
                        null
                },
                new Object[]{
                        "",
                        ""
                },
                new Object[]{
                        "   ",
                        "   "
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] invalidOwnerWithIdCaseData() {
        return new Object[]{
                new Object[]{
                        null,
                        null,
                        null,
                        null
                },
                new Object[]{
                        "",
                        "",
                        "",
                        ""
                },
                new Object[]{
                        "   ",
                        "   ",
                        "   ",
                        "   "
                }
        };
    }


    @SuppressWarnings("unused")
    public Object[] invalidOwnerWithIndexCaseData() {
        return new Object[]{
                new Object[]{
                        null,
                        null,
                        null,
                        null
                },
                new Object[]{
                        "",
                        -1,
                        "",
                        ""
                },
                new Object[]{
                        "   ",
                        -5,
                        "   ",
                        "   "
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] invalidOwnerMainFieldsCaseData() {
        return new Object[]{
                new Object[]{
                        null,
                        null,
                        null
                },
                new Object[]{
                        "",
                        "",
                        ""
                },
                new Object[]{
                        "   ",
                        "   ",
                        "   "
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] invalidOwnerIndexCaseData() {
        return new Object[]{
                new Object[]{
                        null,
                        null
                },
                new Object[]{
                        "",
                        -1
                },
                new Object[]{
                        "   ",
                        -5
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] invalidCollectionsUsersCaseData() {
        return new Object[]{
                new Object[]{null},
                new Object[]{
                        Arrays.asList(
                                null,
                                null,
                                null
                        )
                },
                new Object[]{Collections.emptyList()},
                new Object[]{new ArrayList<>()}
        };
    }

}