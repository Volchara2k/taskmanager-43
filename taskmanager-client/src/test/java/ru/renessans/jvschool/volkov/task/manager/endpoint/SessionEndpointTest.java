package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class SessionEndpointTest {

    @NotNull
    private static final AuthenticationEndpointService AUTHENTICATION_ENDPOINT_SERVICE = new AuthenticationEndpointService();

    @NotNull
    private static final AuthenticationEndpoint AUTH_ENDPOINT = AUTHENTICATION_ENDPOINT_SERVICE.getAuthenticationEndpointPort();

    @NotNull
    private static final SessionEndpointService SESSION_ENDPOINT_SERVICE = new SessionEndpointService();

    @NotNull
    private static final SessionEndpoint SESSION_ENDPOINT = SESSION_ENDPOINT_SERVICE.getSessionEndpointPort();

    @BeforeAll
    static void assertMainComponentsBeforeAll() {
        Assert.assertNotNull(AUTHENTICATION_ENDPOINT_SERVICE);
        Assert.assertNotNull(AUTH_ENDPOINT);
    }

    @Test
    @TestCaseName("Run testVerifyValidSessionState for validateSession(session)")
    public void testVerifyValidSessionState() {
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final SessionValidState verifyValidSessionState = SESSION_ENDPOINT.verifyValidSessionState(open);
        Assert.assertNotNull(verifyValidSessionState);
        Assert.assertEquals(SessionValidState.SUCCESS, verifyValidSessionState);
        @Nullable final SessionDTO closed = AUTH_ENDPOINT.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testVerifyValidPermissionState for verifyValidPermission(session, role)")
    public void testVerifyValidPermissionState() {
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final PermissionValidState permissionValidState =
                SESSION_ENDPOINT.verifyValidPermissionState(open, UserRole.ADMIN);
        Assert.assertNotNull(permissionValidState);
        Assert.assertEquals(PermissionValidState.SUCCESS, permissionValidState);
        @Nullable final SessionDTO closed = AUTH_ENDPOINT.closeSession(open);
        Assert.assertNotNull(closed);
    }

}