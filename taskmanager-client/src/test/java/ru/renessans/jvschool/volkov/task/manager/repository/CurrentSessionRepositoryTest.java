package ru.renessans.jvschool.volkov.task.manager.repository;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidSessionException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;

import java.util.UUID;

public final class CurrentSessionRepositoryTest {

    @NotNull
    private final ICurrentSessionRepository currentSessionRepository = new CurrentSessionRepository();

    @Test(expected = InvalidSessionException.class)
    @TestCaseName("Run testNegativeDelete for delete()")
    @Category({NegativeImplementation.class, RepositoryImplementation.class})
    public void testNegativeDelete() {
        Assert.assertNotNull(this.currentSessionRepository);
        this.currentSessionRepository.delete();
    }

    @Test
    @TestCaseName("Run testPut for put(session)")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testSet() {
        Assert.assertNotNull(this.currentSessionRepository);
        @NotNull final SessionDTO session = new SessionDTO();
        Assert.assertNotNull(session);
        session.setTimestamp(System.currentTimeMillis());
        session.setUserId(UUID.randomUUID().toString());

        @NotNull final SessionDTO setSession = this.currentSessionRepository.set(session);
        Assert.assertNotNull(setSession);
        Assert.assertEquals(session.getId(), setSession.getId());
        Assert.assertEquals(session.getTimestamp(), setSession.getTimestamp());
        Assert.assertEquals(session.getUserId(), setSession.getUserId());
    }

    @Test
    @TestCaseName("Run testGet for get()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGet() {
        Assert.assertNotNull(this.currentSessionRepository);
        @NotNull final SessionDTO session = new SessionDTO();
        Assert.assertNotNull(session);
        session.setTimestamp(System.currentTimeMillis());
        session.setUserId(UUID.randomUUID().toString());
        @NotNull final SessionDTO setSession = this.currentSessionRepository.set(session);
        Assert.assertNotNull(setSession);

        @Nullable final SessionDTO getSession = this.currentSessionRepository.get();
        Assert.assertNotNull(getSession);
        Assert.assertEquals(setSession.getId(), getSession.getId());
        Assert.assertEquals(setSession.getTimestamp(), getSession.getTimestamp());
        Assert.assertEquals(setSession.getUserId(), getSession.getUserId());
        Assert.assertEquals(setSession.getSignature(), getSession.getSignature());
    }

    @Test
    @TestCaseName("Run testDelete for delete()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testDelete() {
        Assert.assertNotNull(this.currentSessionRepository);
        @NotNull final SessionDTO session = new SessionDTO();
        Assert.assertNotNull(session);
        session.setTimestamp(System.currentTimeMillis());
        session.setUserId(UUID.randomUUID().toString());
        @NotNull final SessionDTO putSession = this.currentSessionRepository.set(session);
        Assert.assertNotNull(putSession);

        @Nullable final SessionDTO deleteSession = this.currentSessionRepository.delete();
        Assert.assertNotNull(deleteSession);
        Assert.assertEquals(putSession.getId(), deleteSession.getId());
        Assert.assertEquals(putSession.getTimestamp(), deleteSession.getTimestamp());
        Assert.assertEquals(putSession.getUserId(), deleteSession.getUserId());
        Assert.assertEquals(putSession.getSignature(), deleteSession.getSignature());
    }

}