package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class AuthenticationEndpointTest {

    @NotNull
    private static final AuthenticationEndpointService AUTHENTICATION_ENDPOINT_SERVICE = new AuthenticationEndpointService();

    @NotNull
    private static final AuthenticationEndpoint AUTH_ENDPOINT = AUTHENTICATION_ENDPOINT_SERVICE.getAuthenticationEndpointPort();

    @NotNull
    private static final AdminEndpointService ADMIN_ENDPOINT_SERVICE = new AdminEndpointService();

    @NotNull
    private static final AdminEndpoint ADMIN_ENDPOINT = ADMIN_ENDPOINT_SERVICE.getAdminEndpointPort();

    @BeforeAll
    static void assertMainComponentsBeforeAll() {
        Assert.assertNotNull(AUTHENTICATION_ENDPOINT_SERVICE);
        Assert.assertNotNull(AUTH_ENDPOINT);
        Assert.assertNotNull(ADMIN_ENDPOINT_SERVICE);
        Assert.assertNotNull(ADMIN_ENDPOINT);
    }

    @Test
    @TestCaseName("Run testOpenSession for openSession(login, password)")
    public void testOpenSession() {
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @Nullable final SessionDTO closed = AUTH_ENDPOINT.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testCloseSession for closeSession(session)")
    public void testCloseSession() {
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final SessionDTO closed = AUTH_ENDPOINT.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testSignUpUser for signUpUser(null, random, random)")
    public void testSignUpUser() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUser = AUTH_ENDPOINT.signUpUser(null, login, password);
        Assert.assertNotNull(addUser);
        Assert.assertEquals(addUser.getId(), addUser.getId());
        Assert.assertEquals(login, addUser.getLogin());
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        final int deleteFlag = ADMIN_ENDPOINT.deleteUserByLogin(open, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = AUTH_ENDPOINT.closeSession(open);
        Assert.assertNotNull(closed);
    }

}