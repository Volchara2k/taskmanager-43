package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

import java.util.Collection;
import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class ProjectEndpointTest {

    @NotNull
    private static final AuthenticationEndpointService AUTHENTICATION_ENDPOINT_SERVICE = new AuthenticationEndpointService();

    @NotNull
    private static final AuthenticationEndpoint AUTH_ENDPOINT = AUTHENTICATION_ENDPOINT_SERVICE.getAuthenticationEndpointPort();

    @NotNull
    private static final ProjectEndpointService PROJECT_ENDPOINT_SERVICE = new ProjectEndpointService();

    @NotNull
    private static final ProjectEndpoint PROJECT_ENDPOINT = PROJECT_ENDPOINT_SERVICE.getProjectEndpointPort();

    @BeforeAll
    static void assertMainComponentsBeforeAll() {
        Assert.assertNotNull(AUTHENTICATION_ENDPOINT_SERVICE);
        Assert.assertNotNull(AUTH_ENDPOINT);
        Assert.assertNotNull(PROJECT_ENDPOINT_SERVICE);
        Assert.assertNotNull(PROJECT_ENDPOINT);
    }

    @Test
    @TestCaseName("Run testAddTask for addTask(session, random, random)")
    public void testAddTask() {
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = PROJECT_ENDPOINT.addProject(
                open, title, description
        );
        Assert.assertNotNull(addProject);
        Assert.assertEquals(title, addProject.getTitle());
        Assert.assertEquals(description, addProject.getDescription());
        @Nullable final SessionDTO closed = AUTH_ENDPOINT.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testUpdateTaskByIndex for updateTaskByIndex(session, 0, random, random)")
    public void testUpdateTaskByIndex() {
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        @Nullable final ProjectDTO updateProject =
                PROJECT_ENDPOINT.updateProjectByIndex(open, 0, title, description);
        Assert.assertNotNull(updateProject);
        Assert.assertEquals(title, updateProject.getTitle());
        Assert.assertEquals(description, updateProject.getDescription());
        @Nullable final SessionDTO closed = AUTH_ENDPOINT.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testUpdateTaskById for updateTaskById(session, id, random, random)")
    public void testUpdateTaskById() {
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final ProjectDTO getProject = PROJECT_ENDPOINT.getProjectByIndex(open, 0);
        Assert.assertNotNull(getProject);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        @Nullable final ProjectDTO updateProject = PROJECT_ENDPOINT.updateProjectById(
                open, getProject.getId(), title, description
        );
        Assert.assertNotNull(updateProject);
        Assert.assertEquals(getProject.getId(), updateProject.getId());
        Assert.assertEquals(getProject.getUserId(), updateProject.getUserId());
        Assert.assertEquals(title, updateProject.getTitle());
        Assert.assertEquals(description, updateProject.getDescription());
        @Nullable final SessionDTO closed = AUTH_ENDPOINT.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testDeleteTaskById for deleteTaskById(session, id)")
    public void testDeleteTaskById() {
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_TEST_LOGIN, DemoDataConst.USER_TEST_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = PROJECT_ENDPOINT.addProject(open, title, description);
        Assert.assertNotNull(addProject);

        final int deleteFlag = PROJECT_ENDPOINT.deleteProjectById(open, addProject.getId());
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = AUTH_ENDPOINT.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testDeleteTaskByIndex for deleteTaskByIndex(session, 0)")
    public void testDeleteTaskByIndex() {
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = PROJECT_ENDPOINT.addProject(open, title, description);
        Assert.assertNotNull(addProject);
        @NotNull final ProjectDTO getTask = PROJECT_ENDPOINT.getProjectByIndex(open, 0);
        Assert.assertNotNull(getTask);

        final int deleteFlag = PROJECT_ENDPOINT.deleteProjectByIndex(open, 0);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = AUTH_ENDPOINT.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testDeleteTaskByTitle for deleteTaskByTitle(session, title))")
    public void testDeleteTaskByTitle() {
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = PROJECT_ENDPOINT.addProject(open, title, description);
        Assert.assertNotNull(addProject);

        final int deleteFlag = PROJECT_ENDPOINT.deleteProjectByTitle(open, addProject.getTitle());
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = AUTH_ENDPOINT.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testDeleteAllTasks for deleteAllTasks(session)")
    public void testDeleteAllTasks() {
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = PROJECT_ENDPOINT.addProject(open, title, description);
        Assert.assertNotNull(addProject);

        final int deleteFlag = PROJECT_ENDPOINT.deleteAllProjects(open);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = AUTH_ENDPOINT.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testGetTaskById for getTaskById(session, id))")
    public void testGetTaskById() {
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = PROJECT_ENDPOINT.addProject(open, title, description);
        Assert.assertNotNull(addProject);

        @Nullable final ProjectDTO getProject = PROJECT_ENDPOINT.getProjectById(open, addProject.getId());
        Assert.assertNotNull(getProject);
        Assert.assertEquals(addProject.getId(), getProject.getId());
        Assert.assertEquals(addProject.getUserId(), getProject.getUserId());
        Assert.assertEquals(addProject.getTitle(), getProject.getTitle());
        Assert.assertEquals(addProject.getDescription(), getProject.getDescription());
        @Nullable final SessionDTO closed = AUTH_ENDPOINT.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testGetTaskByIndex for getTaskByIndex(session, 0)")
    public void testGetTaskByIndex() {
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final ProjectDTO getProject = PROJECT_ENDPOINT.getProjectByIndex(open, 0);
        Assert.assertNotNull(getProject);
        Assert.assertEquals(open.getUserId(), getProject.getUserId());
    }

    @Test
    @TestCaseName("Run testGetTaskByTitle for getTaskByTitle(session, title)")
    public void testGetTaskByTitle() {
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = PROJECT_ENDPOINT.addProject(open, title, description);
        Assert.assertNotNull(addProject);

        @Nullable final ProjectDTO getProject = PROJECT_ENDPOINT.getProjectByTitle(open, addProject.getTitle());
        Assert.assertNotNull(getProject);
        Assert.assertEquals(addProject.getId(), getProject.getId());
        Assert.assertEquals(addProject.getUserId(), getProject.getUserId());
        Assert.assertEquals(addProject.getTitle(), getProject.getTitle());
        Assert.assertEquals(addProject.getDescription(), getProject.getDescription());
        @Nullable final SessionDTO closed = AUTH_ENDPOINT.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testGetAllTasks for getAllTasks(session)")
    public void testGetAllTasks() {
        @NotNull final SessionDTO open = AUTH_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final Collection<ProjectDTO> getAllProject = PROJECT_ENDPOINT.getAllProjects(open);
        Assert.assertNotNull(getAllProject);
        Assert.assertNotEquals(0, getAllProject.size());
        final boolean isUserProjects = getAllProject.stream().allMatch(entity -> open.getUserId().equals(entity.getUserId()));
        Assert.assertTrue(isUserProjects);
        @Nullable final SessionDTO closed = AUTH_ENDPOINT.closeSession(open);
        Assert.assertNotNull(closed);
    }

}