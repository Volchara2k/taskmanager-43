package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidSessionException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.CurrentSessionRepository;

import java.util.UUID;

public final class CurrentSessionServiceTest {

    @NotNull
    private final ICurrentSessionRepository sessionRepository = new CurrentSessionRepository();

    @NotNull
    private final ICurrentSessionService currentSessionService = new CurrentSessionService(sessionRepository);

    @Test(expected = InvalidSessionException.class)
    @TestCaseName("Run testNegativeSubscribe for subscribe()")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    public void testNegativeSubscribe() {
        Assert.assertNotNull(this.currentSessionService);
        this.currentSessionService.subscribe(null);
    }

    @Test
    @TestCaseName("Run testGetSession for getSession()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetSession() {
        Assert.assertNotNull(this.currentSessionService);
        @NotNull final SessionDTO session = new SessionDTO();
        Assert.assertNotNull(session);
        session.setTimestamp(System.currentTimeMillis());
        session.setUserId(UUID.randomUUID().toString());
        @NotNull final SessionDTO putSession = this.currentSessionService.subscribe(session);
        Assert.assertNotNull(putSession);

        @Nullable final SessionDTO getSession = this.currentSessionService.getCurrentSession();
        Assert.assertNotNull(getSession);
        Assert.assertEquals(putSession.getId(), getSession.getId());
        Assert.assertEquals(putSession.getTimestamp(), getSession.getTimestamp());
        Assert.assertEquals(putSession.getUserId(), getSession.getUserId());
        Assert.assertEquals(putSession.getSignature(), getSession.getSignature());
    }

    @Test
    @TestCaseName("Run testSubscribe for subscribe()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testSubscribe() {
        Assert.assertNotNull(this.currentSessionService);
        @NotNull final SessionDTO session = new SessionDTO();
        Assert.assertNotNull(session);
        session.setTimestamp(System.currentTimeMillis());
        session.setUserId(UUID.randomUUID().toString());

        @NotNull final SessionDTO putSession = this.currentSessionService.subscribe(session);
        Assert.assertNotNull(putSession);
        @Nullable final SessionDTO getSession = this.currentSessionService.getCurrentSession();
        Assert.assertNotNull(getSession);
        Assert.assertEquals(putSession.getId(), getSession.getId());
        Assert.assertEquals(putSession.getTimestamp(), getSession.getTimestamp());
        Assert.assertEquals(putSession.getUserId(), getSession.getUserId());
        Assert.assertEquals(putSession.getSignature(), getSession.getSignature());
    }

    @Test
    @TestCaseName("Run testUnsubscribe for unsubscribe()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testUnsubscribe() {
        Assert.assertNotNull(this.currentSessionService);
        @NotNull final SessionDTO session = new SessionDTO();
        Assert.assertNotNull(session);
        session.setTimestamp(System.currentTimeMillis());
        session.setUserId(UUID.randomUUID().toString());
        @NotNull final SessionDTO putSession = this.currentSessionService.subscribe(session);
        Assert.assertNotNull(putSession);

        @Nullable final SessionDTO unsubscribeSession = this.currentSessionService.unsubscribe();
        Assert.assertNotNull(unsubscribeSession);
        Assert.assertEquals(putSession.getId(), unsubscribeSession.getId());
        Assert.assertEquals(putSession.getTimestamp(), unsubscribeSession.getTimestamp());
        Assert.assertEquals(putSession.getUserId(), unsubscribeSession.getUserId());
        Assert.assertEquals(putSession.getSignature(), unsubscribeSession.getSignature());
    }

}