package ru.renessans.jvschool.volkov.task.manager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class ProjectDeleteByIdListener extends AbstractProjectListener {

    @NotNull
    private static final String CMD_PROJECT_DELETE_BY_ID = "project-delete-by-id";

    @NotNull
    private static final String DESC_PROJECT_DELETE_BY_ID = "удалить проект по идентификатору";

    @NotNull
    private static final String NOTIFY_PROJECT_DELETE_BY_ID =
            "Происходит попытка инициализации удаления проекта. \n" +
                    "Для удаления проекта по идентификатору введите идентификатор проекта из списка. ";

    public ProjectDeleteByIdListener(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(projectEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_PROJECT_DELETE_BY_ID;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_PROJECT_DELETE_BY_ID;
    }

    @Async
    @Override
    @EventListener(condition = "@projectDeleteByIdListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_PROJECT_DELETE_BY_ID);
        @NotNull final String id = ViewUtil.getLine();
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        final int deleteFlag = super.projectEndpoint.deleteProjectById(current, id);
        ViewUtil.print(deleteFlag);
    }

}