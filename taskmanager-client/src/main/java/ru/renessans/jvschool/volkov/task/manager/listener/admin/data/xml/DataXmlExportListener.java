package ru.renessans.jvschool.volkov.task.manager.listener.admin.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.admin.data.AbstractAdminDataListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class DataXmlExportListener extends AbstractAdminDataListener {

    @NotNull
    private static final String CMD_XML_EXPORT = "data-xml-export";

    @NotNull
    private static final String DESC_XML_EXPORT = "экспортировать домен в xml вид";

    @NotNull
    private static final String NOTIFY_XML_EXPORT = "Происходит процесс выгрузки домена в xml вид...";

    public DataXmlExportListener(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_XML_EXPORT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_XML_EXPORT;
    }

    @Async
    @Override
    @EventListener(condition = "@dataXmlExportListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @NotNull final DomainDTO exportData = super.adminDataInterChangeEndpoint.exportDataXml(current);
        ViewUtil.print(NOTIFY_XML_EXPORT);
        ViewUtil.print(exportData.getUsers());
        ViewUtil.print(exportData.getTasks());
        ViewUtil.print(exportData.getProjects());
    }

}