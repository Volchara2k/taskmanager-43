
package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for editProfileResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="editProfileResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="editedUser" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}userLimitedDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "editProfileResponse", propOrder = {
    "editedUser"
})
public class EditProfileResponse {

    protected UserLimitedDTO editedUser;

    /**
     * Gets the value of the editedUser property.
     * 
     * @return
     *     possible object is
     *     {@link UserLimitedDTO }
     *     
     */
    public UserLimitedDTO getEditedUser() {
        return editedUser;
    }

    /**
     * Sets the value of the editedUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserLimitedDTO }
     *     
     */
    public void setEditedUser(UserLimitedDTO value) {
        this.editedUser = value;
    }

}
