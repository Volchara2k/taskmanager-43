package ru.renessans.jvschool.volkov.task.manager.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

@Component
public abstract class AbstractListener {

    @NotNull
    public abstract String command();

    @Nullable
    public abstract String argument();

    @NotNull
    public abstract String description();

    public abstract void handler(@NotNull final TerminalInputEvent terminalEvent) throws Exception;

    @NotNull
    @Override
    public String toString() {
        @NotNull final StringBuilder result = new StringBuilder();
        result.append("Терминальная команда: ").append(command());
        if (ValidRuleUtil.isNotNullOrEmpty(argument()))
            result.append(", программный аргумент: ").append(argument());
        result.append("\n\t - ").append(description());
        return result.toString();
    }

}