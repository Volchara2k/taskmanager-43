package ru.renessans.jvschool.volkov.task.manager.listener.admin.data.yaml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.admin.data.AbstractAdminDataListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class DataYamlExportListener extends AbstractAdminDataListener {

    @NotNull
    private static final String CMD_YAML_EXPORT = "data-yaml-export";

    @NotNull
    private static final String DESC_YAML_EXPORT = "экспортировать домен в yaml вид";

    @NotNull
    private static final String NOTIFY_YAML_EXPORT =
            "Происходит попытка инициализации процесса выгрузки домена в yaml вид...";

    public DataYamlExportListener(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_YAML_EXPORT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_YAML_EXPORT;
    }

    @Async
    @Override
    @EventListener(condition = "@dataYamlExportListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @NotNull final DomainDTO exportData = adminDataInterChangeEndpoint.exportDataYaml(current);
        ViewUtil.print(NOTIFY_YAML_EXPORT);
        ViewUtil.print(exportData.getUsers());
        ViewUtil.print(exportData.getTasks());
        ViewUtil.print(exportData.getProjects());
    }

}