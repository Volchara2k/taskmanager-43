package ru.renessans.jvschool.volkov.task.manager.listener.admin.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.admin.data.AbstractAdminDataListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class DataBinaryImportListener extends AbstractAdminDataListener {

    @NotNull
    private static final String CMD_BIN_IMPORT = "data-bin-import";

    @NotNull
    private static final String DESC_BIN_IMPORT = "импортировать домен из бинарного вида";

    @NotNull
    private static final String NOTIFY_BIN_IMPORT =
            "Происходит попытка инициализации процесса загрузки домена из бинарного вида...";

    public DataBinaryImportListener(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_BIN_IMPORT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_BIN_IMPORT;
    }

    @Async
    @Override
    @EventListener(condition = "@dataBinaryImportListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @NotNull final DomainDTO importData = super.adminDataInterChangeEndpoint.importDataBin(current);
        ViewUtil.print(NOTIFY_BIN_IMPORT);
        ViewUtil.print(importData.getUsers());
        ViewUtil.print(importData.getTasks());
        ViewUtil.print(importData.getProjects());
    }

}