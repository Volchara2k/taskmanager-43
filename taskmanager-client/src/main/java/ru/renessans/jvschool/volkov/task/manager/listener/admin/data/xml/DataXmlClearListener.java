package ru.renessans.jvschool.volkov.task.manager.listener.admin.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.admin.data.AbstractAdminDataListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class DataXmlClearListener extends AbstractAdminDataListener {

    @NotNull
    private static final String CMD_XML_CLEAR = "data-xml-clear";

    @NotNull
    private static final String DESC_XML_CLEAR = "очистить xml данные";

    @NotNull
    private static final String NOTIFY_XML_CLEAR = "Происходит процесс очищения xml данных...";

    public DataXmlClearListener(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_XML_CLEAR;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_XML_CLEAR;
    }

    @Async
    @Override
    @EventListener(condition = "@dataXmlClearListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        final boolean clear = super.adminDataInterChangeEndpoint.dataXmlClear(current);
        ViewUtil.print(NOTIFY_XML_CLEAR);
        ViewUtil.print(clear);
    }

}