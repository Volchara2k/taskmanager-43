package ru.renessans.jvschool.volkov.task.manager.event;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Getter
@RequiredArgsConstructor
public final class TerminalInputEvent {

    @NotNull
    private final String inputLine;

}