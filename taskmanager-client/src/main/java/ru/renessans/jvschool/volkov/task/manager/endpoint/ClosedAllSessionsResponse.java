
package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for closedAllSessionsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="closedAllSessionsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="closedAllSessions" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "closedAllSessionsResponse", propOrder = {
    "closedAllSessions"
})
public class ClosedAllSessionsResponse {

    protected int closedAllSessions;

    /**
     * Gets the value of the closedAllSessions property.
     * 
     */
    public int getClosedAllSessions() {
        return closedAllSessions;
    }

    /**
     * Sets the value of the closedAllSessions property.
     * 
     */
    public void setClosedAllSessions(int value) {
        this.closedAllSessions = value;
    }

}
