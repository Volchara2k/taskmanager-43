
package ru.renessans.jvschool.volkov.task.manager.endpoint;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for domainDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="domainDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}abstractModelDTO"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="projects" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}projectDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="tasks" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}taskDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="users" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}userUnlimitedDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "domainDTO", propOrder = {
    "projects",
    "tasks",
    "users"
})
public class DomainDTO
    extends AbstractModelDTO
{

    @XmlElement(nillable = true)
    protected List<ProjectDTO> projects;
    @XmlElement(nillable = true)
    protected List<TaskDTO> tasks;
    @XmlElement(nillable = true)
    protected List<UserUnlimitedDTO> users;

    /**
     * Gets the value of the projects property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the projects property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProjects().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProjectDTO }
     * 
     * 
     */
    public List<ProjectDTO> getProjects() {
        if (projects == null) {
            projects = new ArrayList<ProjectDTO>();
        }
        return this.projects;
    }

    /**
     * Gets the value of the tasks property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tasks property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTasks().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TaskDTO }
     * 
     * 
     */
    public List<TaskDTO> getTasks() {
        if (tasks == null) {
            tasks = new ArrayList<TaskDTO>();
        }
        return this.tasks;
    }

    /**
     * Gets the value of the users property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the users property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserUnlimitedDTO }
     * 
     * 
     */
    public List<UserUnlimitedDTO> getUsers() {
        if (users == null) {
            users = new ArrayList<UserUnlimitedDTO>();
        }
        return this.users;
    }

}
