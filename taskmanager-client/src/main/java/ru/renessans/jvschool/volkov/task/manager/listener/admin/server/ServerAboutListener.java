package ru.renessans.jvschool.volkov.task.manager.listener.admin.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.admin.AbstractAdminListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class ServerAboutListener extends AbstractAdminListener {

    @NotNull
    private static final String CMD_SERVER_ABOUT = "server-about";

    @NotNull
    private static final String DESC_SERVER_ABOUT = "получить информацию о сервере (администратор)";

    @NotNull
    private static final String NOTIFY_SERVER_ABOUT =
            "Происходит попытка инициализации просмотра информации о сервере системы...";

    public ServerAboutListener(
            @NotNull final AdminEndpoint adminEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_SERVER_ABOUT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_SERVER_ABOUT;
    }

    @Async
    @Override
    @EventListener(condition = "@serverAboutListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_SERVER_ABOUT);
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @NotNull final String serverAbout = super.adminEndpoint.serverData(current);
        ViewUtil.print(serverAbout);
    }

}