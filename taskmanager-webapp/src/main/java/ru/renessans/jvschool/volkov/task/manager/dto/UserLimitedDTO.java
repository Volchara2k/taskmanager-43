package ru.renessans.jvschool.volkov.task.manager.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.util.StringUtils;

import javax.xml.bind.annotation.XmlType;

@Data
@XmlType
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class UserLimitedDTO extends AbstractDTO {

    private static final long serialVersionUID = 1L;

    @NotNull
    private String login = "";

    @Nullable
    private String firstName = "";

    @Nullable
    private String lastName = "";

    @Nullable
    private String middleName = "";

    @NotNull
    @Override
    public String toString() {
        @NotNull final StringBuilder result = new StringBuilder();
        result.append("Логин: ").append(getLogin());
        if (!StringUtils.isEmpty(getFirstName()))
            result.append(", имя: ").append(getFirstName()).append("\n");
        if (!StringUtils.isEmpty(getLastName()))
            result.append(", фамилия: ").append(getLastName()).append("\n");
        result.append("\nИдентификатор: ").append(super.getId()).append("\n");
        return result.toString();
    }

}