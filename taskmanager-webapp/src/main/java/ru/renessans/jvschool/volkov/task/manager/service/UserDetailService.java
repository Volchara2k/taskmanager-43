package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.dto.SecureUserDTO;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserRepository;

import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public final class UserDetailService implements UserDetailsService {

    @NotNull
    private final IUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(
            @NonNull final String login
    ) throws UsernameNotFoundException {
        @Nullable final User user = this.userRepository.getUserByLogin(login);
        if (Objects.isNull(user)) throw new UsernameNotFoundException(login);
        if (StringUtils.isEmpty(user.getId())) throw new UsernameNotFoundException("Идентификатор не найден");

        return SecureUserDTO.detailBuilder()
                .userDetails(
                        SecureUserDTO.builder()
                                .username(user.getLogin())
                                .password(user.getPasswordHash())
                                .roles(
                                        user.getRoles()
                                                .stream()
                                                .map(userRole -> userRole.getUserRole().toString())
                                                .collect(Collectors.joining())
                                )
                                .accountLocked(user.getLockdown())
                                .build()
                )
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .build();
    }

}