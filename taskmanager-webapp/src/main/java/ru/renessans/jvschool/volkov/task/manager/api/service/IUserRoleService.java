package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.model.UserRole;

import java.util.Collection;
import java.util.Set;

public interface IUserRoleService extends IService<UserRole> {

    @NotNull
    Set<UserRole> addRole(
            @Nullable String userId,
            @Nullable Collection<UserRoleType> userRoleTypes
    );

}