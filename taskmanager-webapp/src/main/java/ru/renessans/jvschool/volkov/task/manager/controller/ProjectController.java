package ru.renessans.jvschool.volkov.task.manager.controller;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.api.controller.IProjectController;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserProjectService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SecureUserDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserOwnerStatus;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.model.Project;

import java.util.Collection;
import java.util.Objects;

@Controller
@RequiredArgsConstructor
public final class ProjectController implements IProjectController {

    @NotNull
    private final IUserProjectService userProjectService;

    @NotNull
    private final IProjectAdapterService projectAdapterService;

    @NotNull
    @GetMapping("/projects")
    @Override
    public ModelAndView index(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-list");
        @NotNull final Collection<Project> projects = this.userProjectService.getUserOwnerAll(userDTO.getId());
        modelAndView.addObject("projects", projects);
        return modelAndView;
    }

    @NotNull
    @GetMapping("/project/create")
    @Override
    public ModelAndView create(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-settable");
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId(userDTO.getId());
        modelAndView.addObject("project", projectDTO);
        modelAndView.addObject("statuses", UserOwnerStatus.values());
        return modelAndView;
    }

    @NotNull
    @PostMapping("/project/create")
    @Override
    public ModelAndView create(
            @ModelAttribute("project") @NotNull final ProjectDTO projectDTO,
            @NotNull final BindingResult result
    ) {
        @Nullable final Project project = this.projectAdapterService.toModel(projectDTO);
        this.userProjectService.addUserOwner(project);
        return new ModelAndView("redirect:/projects");
    }

    @NotNull
    @SneakyThrows
    @GetMapping("/project/view/{id}")
    @Override
    public ModelAndView view(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Project project = this.userProjectService.getUserOwnerById(userDTO.getId(), id);
        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-view");
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    @NotNull
    @GetMapping("/project/delete/{id}")
    @Override
    public ModelAndView delete(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @PathVariable("id") @NotNull final String id
    ) {
        this.userProjectService.deleteUserOwnerById(userDTO.getId(), id);
        return new ModelAndView("redirect:/projects");
    }

    @NotNull
    @SneakyThrows
    @GetMapping("/project/edit/{id}")
    @Override
    public ModelAndView edit(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Project project = this.userProjectService.getUserOwnerById(userDTO.getId(), id);
        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
        @Nullable final ProjectDTO projectDTO = this.projectAdapterService.toDTO(project);
        if (Objects.isNull(projectDTO)) throw new InvalidOwnerUserException();
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-settable");
        modelAndView.addObject("project", projectDTO);
        return modelAndView;
    }

    @NotNull
    @SneakyThrows
    @PostMapping("/project/edit/{id}")
    @Override
    public ModelAndView edit(
            @ModelAttribute("project") @NotNull final ProjectDTO projectDTO,
            @NotNull final BindingResult result
    ) {
        @Nullable final Project project = this.projectAdapterService.toModel(projectDTO);
        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
        if (StringUtils.isEmpty(project.getId())) throw new InvalidIdException();
        this.userProjectService.addUserOwner(project);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/project/view/{id}");
        modelAndView.addObject("id", project.getId());
        return modelAndView;
    }

}