package ru.renessans.jvschool.volkov.task.manager.repository;

import ru.renessans.jvschool.volkov.task.manager.model.UserRole;

public interface IUserRoleRepository extends IRepository<UserRole> {
}