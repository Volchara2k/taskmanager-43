package ru.renessans.jvschool.volkov.task.manager.endpoint.soap;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap.IProjectSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserProjectService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.util.CurrentUserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
@WebService
@RequiredArgsConstructor
public final class ProjectSoapEndpoint implements IProjectSoapEndpoint {

    @NotNull
    private final IUserProjectService projectUserService;

    @NotNull
    private final IProjectAdapterService projectAdapterService;

    @WebMethod
    @WebResult(name = "projectDTO", partName = "projectDTO")
    @Nullable
    @Override
    public ProjectDTO addProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @NotNull final ProjectDTO projectDTO
    ) {
        @Nullable final Project request = this.projectAdapterService.toModel(projectDTO);
        if (Objects.isNull(request)) return null;
        @NotNull final String userid = CurrentUserUtil.getUserId();
        request.setUserId(userid);
        @NotNull final Project response = this.projectUserService.addUserOwner(request);
        return this.projectAdapterService.toDTO(response);
    }

    @WebMethod
    @WebResult(name = "projectDTO", partName = "projectDTO")
    @Nullable
    @Override
    public ProjectDTO updateProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @NotNull final ProjectDTO projectDTO
    ) {
        @Nullable final Project request = this.projectAdapterService.toModel(projectDTO);
        if (Objects.isNull(request)) return null;
        @NotNull final String userid = CurrentUserUtil.getUserId();
        if (StringUtils.isEmpty(request.getUserId())) request.setUserId(userid);
        @NotNull final Project response = this.projectUserService.addUserOwner(request);
        return this.projectAdapterService.toDTO(response);
    }

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteProjectById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final String userid = CurrentUserUtil.getUserId();
        return this.projectUserService.deleteUserOwnerById(userid, id);
    }

    @WebMethod
    @WebResult(name = "projectDTO", partName = "projectDTO")
    @Nullable
    @Override
    public ProjectDTO getProjectById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final String userid = CurrentUserUtil.getUserId();
        @Nullable final Project project = projectUserService.getUserOwnerById(userid, id);
        return this.projectAdapterService.toDTO(project);
    }

    @WebMethod
    @WebResult(name = "projectsDTO", partName = "projectsDTO")
    @NotNull
    @Override
    public Collection<ProjectDTO> getAllProjects() {
        @NotNull final String userid = CurrentUserUtil.getUserId();
        return this.projectUserService.getUserOwnerAll(userid)
                .stream()
                .map(this.projectAdapterService::toDTO)
                .collect(Collectors.toList());
    }

}