package ru.renessans.jvschool.volkov.task.manager.api.endpoint.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;

import java.util.Collection;

public interface IProjectRestEndpoint {

    @NotNull
    @GetMapping
    @ApiOperation(
            value = "Get all projects",
            notes = "Returns a complete list of project details by order of creation."
    )
    Collection<ProjectDTO> getAllProjects();

    @Nullable
    @SneakyThrows
    @GetMapping("/project/view/{id}")
    @ApiOperation(
            value = "Get project by ID",
            notes = "Returns project by unique ID. Unique ID required."
    )
    ProjectDTO getProjectById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of project",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull String id
    );

    @Nullable
    @RequestMapping(
            value = "/project/create",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(
            value = "Create project",
            notes = "Returns created project. Created project required."
    )
    ProjectDTO createProject(
            @ApiParam(
                    name = "projectDTO",
                    type = "ProjectDTO",
                    value = "Created project",
                    required = true
            )
            @RequestBody @NotNull ProjectDTO projectDTO
    );

    @DeleteMapping("/project/delete/{id}")
    @ApiOperation(
            value = "Delete project by id",
            notes = "Returns integer deleted flag: 1 - true, 0 - false. Unique ID required."
    )
    int deleteProjectById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of project",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull String id
    );

    @Nullable
    @SneakyThrows
    @RequestMapping(
            value = "/project/edit",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(
            value = "Edit project",
            notes = "Returns edited project. Edited project required."
    )
    ProjectDTO editProject(
            @ApiParam(
                    name = "projectDTO",
                    type = "ProjectDTO",
                    value = "Edited project",
                    required = true
            )
            @RequestBody @NotNull ProjectDTO projectDTO
    );

}