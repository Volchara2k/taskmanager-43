package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserTaskService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserTaskRepository;

@Service
@Transactional
public final class UserTaskService extends AbstractUserOwnerService<Task> implements IUserTaskService {

    public UserTaskService(
            @NotNull final IUserTaskRepository userTaskRepository,
            @NotNull final IUserService userService
    ) {
        super(userTaskRepository, userService);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task addUserOwner(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (StringUtils.isEmpty(userId)) throw new InvalidUserIdException();
        if (StringUtils.isEmpty(title)) throw new InvalidTitleException();
        if (StringUtils.isEmpty(description)) throw new InvalidDescriptionException();
        @NotNull final Task task = new Task(userId, title, description);
        return super.addUserOwner(task);
    }

}