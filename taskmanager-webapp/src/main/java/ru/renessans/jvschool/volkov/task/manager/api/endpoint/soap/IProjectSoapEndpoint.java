package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import java.util.Collection;

@SuppressWarnings("unused")
public interface IProjectSoapEndpoint {

    @WebMethod
    @WebResult(name = "projectDTO", partName = "projectDTO")
    @Nullable
    ProjectDTO addProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @NotNull ProjectDTO projectDTO
    );

    @WebMethod
    @WebResult(name = "projectDTO", partName = "projectDTO")
    @Nullable
    ProjectDTO updateProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @NotNull ProjectDTO projectDTO
    );

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    int deleteProjectById(
            @WebParam(name = "id", partName = "id") @Nullable String id
    );

    @WebMethod
    @WebResult(name = "projectDTO", partName = "projectDTO")
    @Nullable
    ProjectDTO getProjectById(
            @WebParam(name = "id", partName = "id") @Nullable String id
    );

    @WebMethod
    @WebResult(name = "projectsDTO", partName = "projectsDTO")
    @NotNull
    Collection<ProjectDTO> getAllProjects();

}