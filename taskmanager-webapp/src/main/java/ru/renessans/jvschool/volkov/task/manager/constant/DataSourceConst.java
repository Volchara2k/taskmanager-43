package ru.renessans.jvschool.volkov.task.manager.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DataSourceConst {

    @NotNull
    public static final String DRIVER = Environment.DRIVER;

    @NotNull
    public static final String URL = Environment.URL;

    @NotNull
    public static final String USER = Environment.USER;

    @NotNull
    public static final String PASS = Environment.PASS;

    @NotNull
    public static final String SHOW_SQL = Environment.SHOW_SQL;

    @NotNull
    public static final String FORMAT_SQL = Environment.FORMAT_SQL;

    @NotNull
    public static final String HBM2DDL_AUTO = Environment.HBM2DDL_AUTO;

    @NotNull
    public static final String DIALECT = Environment.DIALECT;

    @NotNull
    public static final String STATEMENT_BATCH_SIZE = Environment.STATEMENT_BATCH_SIZE;

    @NotNull
    public static final String BATCH_VERSIONED_DATA = Environment.BATCH_VERSIONED_DATA;

    @NotNull
    public static final String ORDER_UPDATES = Environment.ORDER_UPDATES;

    @NotNull
    public static final String ORDER_INSERTS = Environment.ORDER_INSERTS;

}