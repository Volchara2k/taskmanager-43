package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;

import java.util.List;

@NoRepositoryBean
public interface IUserOwnerRepository<E extends AbstractModel> extends IRepository<E> {

    @NotNull
    @Query("FROM #{#entityName} ORDER BY creationDate")
    List<E> exportUserOwner();

    @NotNull
    @Query("FROM #{#entityName} WHERE user.id = ?1 ORDER BY creationDate")
    List<E> getUserOwnerAll(
            @NotNull String userId
    );

    @Nullable
    @Query("FROM #{#entityName} WHERE user.id = ?1 AND id = ?2 ORDER BY creationDate")
    E getUserOwnerById(
            @NotNull String userId,
            @NotNull String id
    );

    @Nullable
    @Query("FROM #{#entityName} WHERE user.id = ?1 AND title = ?2 ORDER BY creationDate")
    E getUserOwnerByTitle(
            @NotNull String userId,
            @NotNull String title
    );

    @Query("SELECT COUNT(*) FROM #{#entityName} WHERE user.id = ?1")
    long countUserOwner(
            @NotNull String userId
    );

    int deleteAllByUserId(
            @NotNull String userId
    );

    int deleteByUserIdAndId(
            @NotNull String userId,
            @NotNull String id
    );

    int deleteByUserIdAndTitle(
            @NotNull String userId,
            @NotNull String title
    );

}