package ru.renessans.jvschool.volkov.task.manager.exception.invalid.hash;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidSignatureObjectException extends AbstractException {

    @NotNull
    private static final String EMPTY_OBJECT = "Ошибка! Параметр \"объект для подписи\" отсутствует!\n";

    public InvalidSignatureObjectException() {
        super(EMPTY_OBJECT);
    }

}