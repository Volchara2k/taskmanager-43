package ru.renessans.jvschool.volkov.task.manager.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserOwnerStatus;

import java.io.Serializable;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractUserOwnerDTO extends AbstractDTO {

    @NotNull
    @ApiModelProperty(
            value = "Title",
            example = "title",
            name = "title",
            required = true
    )
    private String title = "";

    @NotNull
    @ApiModelProperty(
            value = "Description",
            example = "description",
            name = "description",
            required = true
    )
    private String description = "";

    @Nullable
    @ApiModelProperty(
            value = "Unique ID of user",
            example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
            name = "userId"
    )
    private String userId;

    @Nullable
    @ApiModelProperty(
            value = "Time frame",
            name = "timeFrame"
    )
    private TimeFrameDTO timeFrame = new TimeFrameDTO();

    @Nullable
    @ApiModelProperty(
            value = "Status",
            example = "NOT_STARTED",
            name = "status"
    )
    private UserOwnerStatus status = UserOwnerStatus.NOT_STARTED;

    @NotNull
    @Override
    public String toString() {
        return "Заголовок задачи: " + getTitle() +
                ", описание задачи - " + getDescription() +
                "\nИдентификатор: " + super.getId() + "\n";
    }

}