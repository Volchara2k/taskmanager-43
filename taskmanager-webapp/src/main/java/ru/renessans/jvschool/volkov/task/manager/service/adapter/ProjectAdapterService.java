package ru.renessans.jvschool.volkov.task.manager.service.adapter;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TimeFrameDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserOwnerStatus;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.TimeFrame;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Service
public final class ProjectAdapterService implements IProjectAdapterService {

    @Nullable
    @Override
    public ProjectDTO toDTO(@Nullable final Project convertible) {
        if (Objects.isNull(convertible)) return null;
        return ProjectDTO.builder()
                .id(convertible.getId())
                .userId(convertible.getUserId())
                .title(convertible.getTitle())
                .description(convertible.getDescription())
                .timeFrame(
                        TimeFrameDTO.builder()
                                .creationDate(convertible.getTimeFrame().getCreationDate())
                                .startDate(convertible.getTimeFrame().getStartDate())
                                .endDate(convertible.getTimeFrame().getEndDate())
                                .build()
                )
                .status(convertible.getStatus())
                .build();
    }

    @Nullable
    @Override
    public Project toModel(@Nullable final ProjectDTO convertible) {
        if (Objects.isNull(convertible)) return null;

        if (StringUtils.isEmpty(convertible.getId())) convertible.setId(UUID.randomUUID().toString());
        if (Objects.isNull(convertible.getTimeFrame()) || Objects.isNull(convertible.getTimeFrame().getCreationDate()))
            convertible.setTimeFrame(
                    TimeFrameDTO.builder()
                            .creationDate(new Date())
                            .startDate(convertible.getTimeFrame().getStartDate())
                            .endDate(convertible.getTimeFrame().getEndDate())
                            .build()
            );
        if (Objects.isNull(convertible.getStatus())) convertible.setStatus(UserOwnerStatus.NOT_STARTED);

        return Project.builder()
                .id(convertible.getId())
                .userId(convertible.getUserId())
                .title(convertible.getTitle())
                .description(convertible.getDescription())
                .timeFrame(
                        TimeFrame.builder()
                                .creationDate(Objects.requireNonNull(convertible.getTimeFrame().getCreationDate()))
                                .startDate(convertible.getTimeFrame().getStartDate())
                                .endDate(convertible.getTimeFrame().getEndDate())
                                .build()
                )
                .status(convertible.getStatus())
                .build();
    }

}