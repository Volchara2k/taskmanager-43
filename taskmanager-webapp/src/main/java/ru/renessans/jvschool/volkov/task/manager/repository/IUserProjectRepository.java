package ru.renessans.jvschool.volkov.task.manager.repository;

import ru.renessans.jvschool.volkov.task.manager.model.Project;

public interface IUserProjectRepository extends IUserOwnerRepository<Project> {
}