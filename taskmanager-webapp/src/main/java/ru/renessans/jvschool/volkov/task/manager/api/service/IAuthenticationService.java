package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Set;

public interface IAuthenticationService {

    @NotNull
    User signUp(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    User signUp(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    User signUp(
            @Nullable String login,
            @Nullable String password,
            @Nullable Set<UserRoleType> userRoleTypes
    );

}