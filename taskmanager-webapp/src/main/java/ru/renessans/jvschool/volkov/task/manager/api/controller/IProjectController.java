package ru.renessans.jvschool.volkov.task.manager.api.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SecureUserDTO;

public interface IProjectController {

    @NotNull
    @GetMapping("/projects")
    ModelAndView index(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO
    );

    @NotNull
    @GetMapping("/project/create")
    ModelAndView create(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO
    );

    @NotNull
    @PostMapping("/project/create")
    ModelAndView create(
            @ModelAttribute("project") @NotNull ProjectDTO projectDTO,
            @NotNull BindingResult result
    );

    @NotNull
    @SneakyThrows
    @GetMapping("/project/view/{id}")
    ModelAndView view(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO,
            @PathVariable("id") @NotNull String id
    );

    @NotNull
    @GetMapping("/project/delete/{id}")
    ModelAndView delete(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO,
            @PathVariable("id") @NotNull String id
    );

    @NotNull
    @SneakyThrows
    @GetMapping("/project/edit/{id}")
    ModelAndView edit(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO,
            @PathVariable("id") @NotNull String id
    );

    @NotNull
    @PostMapping("/project/edit/{id}")
    ModelAndView edit(
            @ModelAttribute("project") @NotNull ProjectDTO projectDTO,
            @NotNull BindingResult result
    );

}