package ru.renessans.jvschool.volkov.task.manager.service.adapter;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserLimitedAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Objects;
import java.util.UUID;

@Service
public final class UserLimitedAdapterService implements IUserLimitedAdapterService {

    @Nullable
    @Override
    public UserLimitedDTO toDTO(@Nullable final User convertible) {
        if (Objects.isNull(convertible)) return null;
        return UserLimitedDTO.builder()
                .id(convertible.getId())
                .login(convertible.getLogin())
                .firstName(convertible.getFirstName())
                .lastName(convertible.getLastName())
                .middleName(convertible.getMiddleName())
                .build();
    }

    @Nullable
    @Override
    public User toModel(@Nullable final UserLimitedDTO convertible) {
        if (Objects.isNull(convertible)) return null;
        return User.builder()
                .id(
                        !StringUtils.isEmpty(convertible.getId())
                                ? convertible.getId() : UUID.randomUUID().toString()
                )
                .login(convertible.getLogin())
                .firstName(convertible.getFirstName())
                .lastName(convertible.getLastName())
                .middleName(convertible.getMiddleName())
                .build();
    }

}