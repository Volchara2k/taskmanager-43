package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;

@NoRepositoryBean
public interface IRepository<E extends AbstractModel> extends JpaRepository<E, String> {

    @Nullable
    @Query("FROM #{#entityName} WHERE id = ?1")
    E getRecordById(@NotNull String id);

}