package ru.renessans.jvschool.volkov.task.manager.casedata;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.UUID;

@SuppressWarnings("unused")
public final class CaseDataUserProvider {

    @NotNull
    public static User createUser() {
        return new User(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString()
        );
    }

}