package ru.renessans.jvschool.volkov.task.manager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserProjectService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataProjectProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Collection;
import java.util.UUID;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
@Category({PositiveImplementation.class, ServiceImplementation.class})
public class ProjectServiceTest {

    @Autowired
    private IUserProjectService userProjectService;

    @Autowired
    private IUserService userService;

    @Test
    public void testAdd() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        project.setUserId(user.getId());
        project.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);

        @NotNull final Project addTaskResponse = this.userProjectService.addUserOwner(user.getId(), project.getTitle(), project.getDescription());
        Assert.assertNotNull(addTaskResponse);
        Assert.assertEquals(project.getUserId(), addTaskResponse.getUserId());
        Assert.assertEquals(project.getTitle(), addTaskResponse.getTitle());
        Assert.assertEquals(project.getDescription(), addTaskResponse.getDescription());
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        project.setUserId(user.getId());
        project.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project addProjectResponse = this.userProjectService.addUserOwner(user.getId(), project.getTitle(), project.getDescription());
        Assert.assertNotNull(addProjectResponse);

        @NotNull final String newData = UUID.randomUUID().toString();
        Assert.assertNotNull(newData);
        @Nullable final Project updateProjectResponse = this.userProjectService.updateUserOwnerByIndex(saveUserResponse.getId(), 0, newData, newData);
        Assert.assertNotNull(updateProjectResponse);
        Assert.assertEquals(addProjectResponse.getId(), updateProjectResponse.getId());
        Assert.assertEquals(addProjectResponse.getUserId(), updateProjectResponse.getUserId());
        Assert.assertEquals(newData, updateProjectResponse.getTitle());
        Assert.assertEquals(newData, updateProjectResponse.getDescription());
    }

    @Test
    public void testUpdateById() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        project.setUserId(user.getId());
        project.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project addProjectResponse = this.userProjectService.addUserOwner(user.getId(), project.getTitle(), project.getDescription());
        Assert.assertNotNull(addProjectResponse);

        @NotNull final String newData = UUID.randomUUID().toString();
        Assert.assertNotNull(newData);
        @Nullable final Project updateProjectResponse = this.userProjectService.updateUserOwnerById(saveUserResponse.getId(), addProjectResponse.getId(), newData, newData);
        Assert.assertNotNull(updateProjectResponse);
        Assert.assertEquals(addProjectResponse.getId(), updateProjectResponse.getId());
        Assert.assertEquals(addProjectResponse.getUserId(), updateProjectResponse.getUserId());
        Assert.assertEquals(newData, updateProjectResponse.getTitle());
        Assert.assertEquals(newData, updateProjectResponse.getDescription());
    }

    @Test
    public void testDeleteByIndex() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        project.setUserId(user.getId());
        project.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project addProjectResponse = this.userProjectService.addUserOwner(user.getId(), project.getTitle(), project.getDescription());
        Assert.assertNotNull(addProjectResponse);

        final int deleteProjectResponse = this.userProjectService.deleteUserOwnerByIndex(saveUserResponse.getId(), 0);
        Assert.assertEquals(1, deleteProjectResponse);
    }

    @Test
    public void testDeleteById() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        project.setUserId(user.getId());
        project.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project addProjectResponse = this.userProjectService.addUserOwner(user.getId(), project.getTitle(), project.getDescription());
        Assert.assertNotNull(addProjectResponse);

        final int deleteProjectResponse = this.userProjectService.deleteUserOwnerById(saveUserResponse.getId(), addProjectResponse.getId());
        Assert.assertEquals(1, deleteProjectResponse);
    }

    @Test
    public void testDeleteByTitle() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        project.setUserId(user.getId());
        project.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project addProjectResponse = this.userProjectService.addUserOwner(user.getId(), project.getTitle(), project.getDescription());
        Assert.assertNotNull(addProjectResponse);

        final int deleteProjectResponse = this.userProjectService.deleteUserOwnerByTitle(saveUserResponse.getId(), addProjectResponse.getTitle());
        Assert.assertEquals(1, deleteProjectResponse);
    }

    @Test
    public void testDeleteAll() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        project.setUserId(user.getId());
        project.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project addProjectResponse = this.userProjectService.addUserOwner(user.getId(), project.getTitle(), project.getDescription());
        Assert.assertNotNull(addProjectResponse);

        final int deleteProjectsResponse = this.userProjectService.deleteUserOwnerAll(saveUserResponse.getId());
        Assert.assertEquals(1, deleteProjectsResponse);
    }

    @Test
    public void testGetByIndex() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        project.setUserId(user.getId());
        project.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project addProjectResponse = this.userProjectService.addUserOwner(user.getId(), project.getTitle(), project.getDescription());
        Assert.assertNotNull(addProjectResponse);

        @Nullable final Project projectResponse = this.userProjectService.getUserOwnerByIndex(saveUserResponse.getId(), 0);
        Assert.assertNotNull(projectResponse);
        Assert.assertEquals(addProjectResponse.getId(), projectResponse.getId());
        Assert.assertEquals(addProjectResponse.getUserId(), projectResponse.getUserId());
        Assert.assertEquals(addProjectResponse.getTitle(), projectResponse.getTitle());
        Assert.assertEquals(addProjectResponse.getDescription(), projectResponse.getDescription());
    }

    @Test
    public void testGetById() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        project.setUserId(user.getId());
        project.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project addProjectResponse = this.userProjectService.addUserOwner(user.getId(), project.getTitle(), project.getDescription());
        Assert.assertNotNull(addProjectResponse);

        @Nullable final Project projectResponse = this.userProjectService.getUserOwnerById(saveUserResponse.getId(), addProjectResponse.getId());
        Assert.assertNotNull(projectResponse);
        Assert.assertEquals(addProjectResponse.getId(), projectResponse.getId());
        Assert.assertEquals(addProjectResponse.getUserId(), projectResponse.getUserId());
        Assert.assertEquals(addProjectResponse.getTitle(), projectResponse.getTitle());
        Assert.assertEquals(addProjectResponse.getDescription(), projectResponse.getDescription());
    }

    @Test
    public void testGetByTitle() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        project.setUserId(user.getId());
        project.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project addProjectResponse = this.userProjectService.addUserOwner(user.getId(), project.getTitle(), project.getDescription());
        Assert.assertNotNull(addProjectResponse);

        @Nullable final Project projectResponse = this.userProjectService.getUserOwnerByTitle(saveUserResponse.getId(), addProjectResponse.getTitle());
        Assert.assertNotNull(projectResponse);
        Assert.assertEquals(addProjectResponse.getId(), projectResponse.getId());
        Assert.assertEquals(addProjectResponse.getUserId(), projectResponse.getUserId());
        Assert.assertEquals(addProjectResponse.getTitle(), projectResponse.getTitle());
        Assert.assertEquals(addProjectResponse.getDescription(), projectResponse.getDescription());
    }

    @Test
    public void testGetAll() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        project.setUserId(user.getId());
        project.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project addProjectResponse = this.userProjectService.addUserOwner(user.getId(), project.getTitle(), project.getDescription());
        Assert.assertNotNull(addProjectResponse);

        @Nullable final Collection<Project> projectsResponse = this.userProjectService.getUserOwnerAll(saveUserResponse.getId());
        Assert.assertNotNull(projectsResponse);
        Assert.assertNotEquals(0, projectsResponse.size());
        final boolean isUserProjects = projectsResponse.stream().allMatch(entity -> {
            assert saveUserResponse.getId() != null;
            return saveUserResponse.getId().equals(entity.getUserId());
        });
        Assert.assertTrue(isUserProjects);
    }

}