package ru.renessans.jvschool.volkov.task.manager.runner;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.controller.view.ProjectControllerTest;
import ru.renessans.jvschool.volkov.task.manager.controller.view.TaskControllerTest;
import ru.renessans.jvschool.volkov.task.manager.marker.ControllerImplementation;

@RunWith(Categories.class)
@Categories.IncludeCategory(ControllerImplementation.class)
@Suite.SuiteClasses(
        {
                ProjectControllerTest.class,
                TaskControllerTest.class
        }
)

public abstract class AbstractControllerImplementationRunner {
}