package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataProjectProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.configuration.DataSourceConfiguration;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Collection;

@Transactional
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = DataSourceConfiguration.class)
@Category({PositiveImplementation.class, RepositoryImplementation.class})
public class ProjectRepositoryTest {

    @Autowired
    private IUserProjectRepository projectRepository;

    @Autowired
    private IUserRepository userRepository;

    @Test
    public void testAdd() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        @NotNull final User saveUserResponse = this.userRepository.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setUser(user);

        @NotNull final Project addRecordResponse = this.projectRepository.save(project);
        Assert.assertNotNull(addRecordResponse);
        Assert.assertEquals(project.getId(), addRecordResponse.getId());
        Assert.assertEquals(project.getUserId(), addRecordResponse.getUserId());
        Assert.assertEquals(project.getTitle(), addRecordResponse.getTitle());
        Assert.assertEquals(project.getDescription(), addRecordResponse.getDescription());
    }

    @Test
    public void testGetAll() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        @NotNull final User saveUserResponse = this.userRepository.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setUser(user);
        @NotNull final Project addRecordResponse = this.projectRepository.save(project);
        Assert.assertNotNull(addRecordResponse);

        @Nullable final Collection<Project> projectsResponse = this.projectRepository.getUserOwnerAll(user.getId());
        System.out.println(projectsResponse.stream());
        Assert.assertNotNull(projectsResponse);
        Assert.assertNotEquals(0, projectsResponse.size());
        final boolean isUserProjects = projectsResponse.stream().allMatch(entity -> {
            assert entity.getUser() != null;
            return user.getId().equals(entity.getUser().getId());
        });
        Assert.assertTrue(isUserProjects);
    }

    @Test
    public void testGetById() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        @NotNull final User saveUserResponse = this.userRepository.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setUser(user);
        @NotNull final Project addRecordResponse = this.projectRepository.save(project);
        Assert.assertNotNull(addRecordResponse);

        @Nullable final Project projectResponse = this.projectRepository.getUserOwnerById(user.getId(), project.getId());
        Assert.assertNotNull(projectResponse);
        Assert.assertEquals(project.getId(), projectResponse.getId());
        Assert.assertEquals(project.getUserId(), projectResponse.getUserId());
        Assert.assertEquals(project.getTitle(), projectResponse.getTitle());
        Assert.assertEquals(project.getDescription(), projectResponse.getDescription());
    }

    @Test
    public void testGetByTitle() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        @NotNull final User saveUserResponse = this.userRepository.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setUser(user);
        @NotNull final Project addRecordResponse = this.projectRepository.save(project);
        Assert.assertNotNull(addRecordResponse);

        @Nullable final Project projectResponse = this.projectRepository.getUserOwnerByTitle(user.getId(), project.getTitle());
        Assert.assertNotNull(projectResponse);
        Assert.assertEquals(project.getId(), projectResponse.getId());
        Assert.assertEquals(project.getUserId(), projectResponse.getUserId());
        Assert.assertEquals(project.getTitle(), projectResponse.getTitle());
        Assert.assertEquals(project.getDescription(), projectResponse.getDescription());
    }

    @Test
    public void testDeleteById() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        @NotNull final User saveUserResponse = this.userRepository.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setUser(user);
        @NotNull final Project addRecordResponse = this.projectRepository.save(project);
        Assert.assertNotNull(addRecordResponse);

        final int deleteProjectResponse = this.projectRepository.deleteByUserIdAndId(user.getId(), project.getId());
        Assert.assertEquals(0, deleteProjectResponse);
    }

    @Test
    public void testDeleteByTitle() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        @NotNull final User saveUserResponse = this.userRepository.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setUser(user);
        @NotNull final Project addRecordResponse = this.projectRepository.save(project);
        Assert.assertNotNull(addRecordResponse);

        final int deleteProjectResponse = this.projectRepository.deleteByUserIdAndTitle(user.getId(), project.getTitle());
        Assert.assertEquals(0, deleteProjectResponse);
    }

    @Test
    public void testDeleteAll() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        @NotNull final User saveUserResponse = this.userRepository.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Project project = CaseDataProjectProvider.createProject();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setUser(user);
        @NotNull final Project addRecordResponse = this.projectRepository.save(project);
        Assert.assertNotNull(addRecordResponse);

        final int deleteProjectsResponse = this.projectRepository.deleteAllByUserId(user.getId());
        Assert.assertEquals(0, deleteProjectsResponse);
    }

}