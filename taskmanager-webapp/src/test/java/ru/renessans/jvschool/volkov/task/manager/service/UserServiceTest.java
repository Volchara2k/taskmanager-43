package ru.renessans.jvschool.volkov.task.manager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.model.UserRole;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Transactional
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@Category({PositiveImplementation.class, ServiceImplementation.class})
public class UserServiceTest {

    @Autowired
    private IUserService userService;

    @Test
    public void testAddUser() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final User addUserResponse = this.userService.addUser(login, UUID.randomUUID().toString());
        Assert.assertNotNull(addUserResponse);
        Assert.assertEquals(login, addUserResponse.getLogin());
    }

    @Test
    public void testAddUserWithFirstName() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String firstName = UUID.randomUUID().toString();
        @NotNull final User addUserResponse = this.userService.addUser(login, UUID.randomUUID().toString(), firstName);
        Assert.assertNotNull(addUserResponse);
        Assert.assertEquals(login, addUserResponse.getLogin());
        Assert.assertEquals(firstName, addUserResponse.getFirstName());
    }

    @Test
    public void testAddUserWithRole() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final Set<UserRoleType> roles = new HashSet<>(Collections.singleton(UserRoleType.MANAGER));
        @NotNull final User addUserResponse = this.userService.addUser(login, UUID.randomUUID().toString(), roles);
        Assert.assertNotNull(addUserResponse);
        Assert.assertEquals(login, addUserResponse.getLogin());
        Assert.assertEquals(
                roles, addUserResponse.getRoles()
                        .stream()
                        .map(UserRole::getUserRole)
                        .collect(Collectors.toSet())
        );
    }

}