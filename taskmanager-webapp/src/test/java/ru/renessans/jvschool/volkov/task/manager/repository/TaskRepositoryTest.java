package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataTaskProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.configuration.DataSourceConfiguration;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Collection;

@Transactional
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = DataSourceConfiguration.class)
@Category({PositiveImplementation.class, RepositoryImplementation.class})
public class TaskRepositoryTest {

    @Autowired
    private IUserTaskRepository taskRepository;

    @Autowired
    private IUserRepository userRepository;

    @Test
    public void testAdd() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        @NotNull final User saveUserResponse = this.userRepository.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setUser(user);

        @NotNull final Task addRecordResponse = this.taskRepository.save(task);
        Assert.assertNotNull(addRecordResponse);
        Assert.assertEquals(task.getId(), addRecordResponse.getId());
        Assert.assertEquals(task.getUserId(), addRecordResponse.getUserId());
        Assert.assertEquals(task.getTitle(), addRecordResponse.getTitle());
        Assert.assertEquals(task.getDescription(), addRecordResponse.getDescription());
    }

    @Test
    public void testGetAll() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        @NotNull final User saveUserResponse = this.userRepository.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setUser(user);
        @NotNull final Task addRecordResponse = this.taskRepository.save(task);
        Assert.assertNotNull(addRecordResponse);

        @Nullable final Collection<Task> tasksResponse = this.taskRepository.getUserOwnerAll(user.getId());
        System.out.println(tasksResponse.stream());
        Assert.assertNotNull(tasksResponse);
        Assert.assertNotEquals(0, tasksResponse.size());
        final boolean isUserTasks = tasksResponse.stream().allMatch(entity -> {
            assert entity.getUser() != null;
            return user.getId().equals(entity.getUser().getId());
        });
        Assert.assertTrue(isUserTasks);
    }

    @Test
    public void testGetById() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        @NotNull final User saveUserResponse = this.userRepository.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setUser(user);
        @NotNull final Task addRecordResponse = this.taskRepository.save(task);
        Assert.assertNotNull(addRecordResponse);

        @Nullable final Task taskResponse = this.taskRepository.getUserOwnerById(user.getId(), task.getId());
        Assert.assertNotNull(taskResponse);
        Assert.assertEquals(task.getId(), taskResponse.getId());
        Assert.assertEquals(task.getUserId(), taskResponse.getUserId());
        Assert.assertEquals(task.getTitle(), taskResponse.getTitle());
        Assert.assertEquals(task.getDescription(), taskResponse.getDescription());
    }

    @Test
    public void testGetByTitle() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        @NotNull final User saveUserResponse = this.userRepository.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setUser(user);
        @NotNull final Task addRecordResponse = this.taskRepository.save(task);
        Assert.assertNotNull(addRecordResponse);

        @Nullable final Task taskResponse = this.taskRepository.getUserOwnerByTitle(user.getId(), task.getTitle());
        Assert.assertNotNull(taskResponse);
        Assert.assertEquals(task.getId(), taskResponse.getId());
        Assert.assertEquals(task.getUserId(), taskResponse.getUserId());
        Assert.assertEquals(task.getTitle(), taskResponse.getTitle());
        Assert.assertEquals(task.getDescription(), taskResponse.getDescription());
    }

    @Test
    public void testDeleteById() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        @NotNull final User saveUserResponse = this.userRepository.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setUser(user);
        @NotNull final Task addRecordResponse = this.taskRepository.save(task);
        Assert.assertNotNull(addRecordResponse);

        final int deleteTaskResponse = this.taskRepository.deleteByUserIdAndId(user.getId(), task.getId());
        Assert.assertEquals(0, deleteTaskResponse);
    }

    @Test
    public void testDeleteByTitle() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        @NotNull final User saveUserResponse = this.userRepository.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setUser(user);
        @NotNull final Task addRecordResponse = this.taskRepository.save(task);
        Assert.assertNotNull(addRecordResponse);

        final int deleteTaskResponse = this.taskRepository.deleteByUserIdAndTitle(user.getId(), task.getTitle());
        Assert.assertEquals(0, deleteTaskResponse);
    }

    @Test
    public void testDeleteAll() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        @NotNull final User saveUserResponse = this.userRepository.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setUser(user);
        @NotNull final Task addRecordResponse = this.taskRepository.save(task);
        Assert.assertNotNull(addRecordResponse);

        final int deleteTasksResponse = this.taskRepository.deleteAllByUserId(user.getId());
        Assert.assertEquals(0, deleteTasksResponse);
    }

}