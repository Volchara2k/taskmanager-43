package ru.renessans.jvschool.volkov.task.manager.casedata;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.model.Project;

import java.util.UUID;

public final class CaseDataProjectProvider {

    @NotNull
    public static Project createProject() {
        return new Project(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString()
        );
    }

}