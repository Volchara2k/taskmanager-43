package ru.renessans.jvschool.volkov.task.manager.controller.view;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.marker.ControllerImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Category({PositiveImplementation.class, ControllerImplementation.class})
public class TaskControllerTest extends AbstractWebApplicationTest {

    @Test
    public void tasksTest() throws Exception {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get("/tasks"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("task/task-list"));
    }

    @Test
    public void viewTest() throws Exception {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get("/task/view/{id}", TASK_1ST.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("task/task-view"));
    }

    @Test
    public void createGetTest() throws Exception {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get("/task/create"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("task/task-settable"));
    }

    @Test
    public void createPostTest() throws Exception {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setTitle("title");
        taskDTO.setDescription("description");
        taskDTO.setProjectId(PROJECT_1ST.getId());
        taskDTO.setUserId(USER.getId());
        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/task/create")
                .flashAttr("task", taskDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/tasks"));
    }

    @Test
    public void removeTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/task/delete/{id}", TASK_1ST.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/tasks"));
    }

    @Test
    public void editGetTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/task/edit/{id}", TASK_1ST.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("task/task-settable"));
    }

    @Test
    public void editPostTest() throws Exception {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setTitle("title");
        taskDTO.setDescription("description");
        taskDTO.setUserId(USER.getId());
        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/task/edit/{id}", TASK_1ST.getId())
                .flashAttr("task", taskDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/task/view/{id}"));
    }

}