package ru.renessans.jvschool.volkov.task.manager;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.runner.AbstractEndpointImplementationRunner;
import ru.renessans.jvschool.volkov.task.manager.runner.AbstractRepositoryImplementationRunner;
import ru.renessans.jvschool.volkov.task.manager.runner.AbstractServiceImplementationRunner;
import ru.renessans.jvschool.volkov.task.manager.runner.AbstractControllerImplementationRunner;

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                AbstractControllerImplementationRunner.class,
                AbstractEndpointImplementationRunner.class,
                AbstractRepositoryImplementationRunner.class,
                AbstractServiceImplementationRunner.class
        }
)

public abstract class AbstractTestRunner {
}