package ru.renessans.jvschool.volkov.task.manager.runner;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.endpoint.rest.ProjectRestEndpointTest;
import ru.renessans.jvschool.volkov.task.manager.endpoint.rest.TaskRestEndpointTest;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;

@RunWith(Categories.class)
@Categories.IncludeCategory(EndpointImplementation.class)
@Suite.SuiteClasses(
        {
                ProjectRestEndpointTest.class,
                TaskRestEndpointTest.class
        }
)

public abstract class AbstractEndpointImplementationRunner {
}