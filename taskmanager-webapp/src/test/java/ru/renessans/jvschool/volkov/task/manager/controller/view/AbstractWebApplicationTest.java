package ru.renessans.jvschool.volkov.task.manager.controller.view;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.configuration.WebApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserProjectRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserTaskRepository;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public abstract class AbstractWebApplicationTest {

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Autowired
    protected IUserService userService;

    @Autowired
    protected IUserTaskRepository userTaskRepository;

    @Autowired
    protected IUserProjectRepository userProjectRepository;

    @Autowired
    protected AuthenticationManager authenticationManager;

    @NotNull
    protected static final Project PROJECT_1ST = new Project();

    @NotNull
    protected static final Project PROJECT_2ND = new Project();

    @NotNull
    protected static final Task TASK_1ST = new Task();

    @NotNull
    protected static final Task TASK_2ND = new Task();

    protected static User USER;

    protected MockMvc mockMvc;

    @BeforeClass
    public static void initialDataBeforeClass() {
        @NotNull final String project1st = "project1st";
        PROJECT_1ST.setTitle(project1st);
        PROJECT_1ST.setDescription(project1st);

        @NotNull final String project2nd = "project2nd";
        PROJECT_2ND.setTitle(project2nd);
        PROJECT_2ND.setDescription(project2nd);

        @NotNull final String task1st = "task1st";
        TASK_1ST.setTitle(task1st);
        TASK_1ST.setDescription(task1st);
        TASK_1ST.setProject(PROJECT_1ST);

        @NotNull final String task2nd = "task2nd";
        TASK_2ND.setTitle(task2nd);
        TASK_2ND.setDescription(task2nd);
        TASK_2ND.setProject(PROJECT_1ST);
    }

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.webApplicationContext)
                .build();
        USER = this.userService.getUserByLogin("test");
        PROJECT_1ST.setUser(USER);
        PROJECT_2ND.setUser(USER);
        TASK_1ST.setUser(USER);
        TASK_2ND.setUser(USER);
        this.userProjectRepository.save(PROJECT_1ST);
        this.userProjectRepository.save(PROJECT_2ND);
        this.userTaskRepository.save(TASK_1ST);
        this.userTaskRepository.save(TASK_2ND);

        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                "test", "test"
        );
        @NotNull final Authentication authentication = this.authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

}