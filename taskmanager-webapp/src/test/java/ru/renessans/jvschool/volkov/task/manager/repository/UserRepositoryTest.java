package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.configuration.DataSourceConfiguration;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.User;

@Transactional
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = DataSourceConfiguration.class)
@Category({PositiveImplementation.class, RepositoryImplementation.class})
public class UserRepositoryTest {

    @Autowired
    private IUserRepository userRepository;

    @Before
    public void assertMainComponentsBefore() {
        Assert.assertNotNull(userRepository);
    }

    @Test
    public void testGetByLogin() {
        @NotNull final User temp = CaseDataUserProvider.createUser();
        Assert.assertNotNull(temp);
        @NotNull final User addUserResponse = this.userRepository.save(temp);
        Assert.assertNotNull(addUserResponse);

        @Nullable final User userResponse = this.userRepository.getUserByLogin(addUserResponse.getLogin());
        Assert.assertNotNull(userResponse);
        Assert.assertEquals(addUserResponse.getId(), userResponse.getId());
        Assert.assertEquals(addUserResponse.getLogin(), userResponse.getLogin());
        Assert.assertEquals(addUserResponse.getPasswordHash(), userResponse.getPasswordHash());
        Assert.assertNotEquals(0, this.userRepository.count());
    }

    @Test
    public void testDeleteByLogin() {
        @NotNull final User temp = CaseDataUserProvider.createUser();
        Assert.assertNotNull(temp);
        @NotNull final User addRecordResponse = this.userRepository.save(temp);
        Assert.assertNotNull(addRecordResponse);
        Assert.assertNotEquals(0, this.userRepository.count());

        final int deleteUserResponse = this.userRepository.deleteByLogin(addRecordResponse.getLogin());
        Assert.assertEquals(1, deleteUserResponse);
    }

    @Test
    public void existsByLogin() {
        @NotNull final User temp = CaseDataUserProvider.createUser();
        Assert.assertNotNull(temp);
        @NotNull final User addRecordResponse = this.userRepository.save(temp);
        Assert.assertNotNull(addRecordResponse);

        final boolean existsLoginResponse = this.userRepository.existsByLogin(addRecordResponse.getLogin());
        Assert.assertTrue(existsLoginResponse);
    }

}