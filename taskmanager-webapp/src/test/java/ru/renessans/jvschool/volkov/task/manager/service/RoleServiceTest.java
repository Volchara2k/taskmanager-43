package ru.renessans.jvschool.volkov.task.manager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserRoleService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.model.UserRole;

import java.util.Collections;
import java.util.Set;
import java.util.UUID;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@Category({PositiveImplementation.class, ServiceImplementation.class})
public class RoleServiceTest {

    @Autowired
    private IUserRoleService userRoleService;

    @Autowired
    private IUserService userService;

    @Test
    public void addRoleTest() {
        @NotNull final User user = new User(
                UUID.randomUUID().toString(), UUID.randomUUID().toString()
        );
        Assert.assertNotNull(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);

        @NotNull final Set<UserRole> userRoleSetResponse = this.userRoleService.addRole(
                saveUserResponse.getId(), Collections.singleton(UserRoleType.MANAGER)
        );
        Assert.assertNotNull(userRoleSetResponse);
        Assert.assertNotEquals(0, userRoleSetResponse.size());
    }

}