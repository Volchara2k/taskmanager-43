package ru.renessans.jvschool.volkov.task.manager.listener.security;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICookieService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.AuthenticationSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class UserLogOutListener extends AbstractSecurityListener {

    @NotNull
    private static final String CMD_LOG_OUT = "log-out";

    @Nullable
    private static final String DESC_LOG_OUT = "выйти из системы";

    @NotNull
    private static final String NOTIFY_LOG_OUT = "Производится выход пользователя из системы...";

    public UserLogOutListener(
            @NotNull final AuthenticationSoapEndpoint sessionEndpoint,
            @NotNull final ICookieService sessionService
    ) {
        super(sessionEndpoint, sessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_LOG_OUT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_LOG_OUT;
    }

    @Async
    @Override
    @EventListener(condition = "@userLogOutListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_LOG_OUT);
        final boolean logoutResponse = super.authenticationEndpoint.logout();
        super.sessionService.clearCookieHeaders();
        ViewUtil.print(logoutResponse);
    }

}