package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICookieRepository;

import java.util.Collections;
import java.util.List;

@Repository
public final class CookieRepository implements ICookieRepository {

    @NotNull
    private List<String> cookieHeaders = Collections.emptyList();

    @NotNull
    @Override
    public List<String> getCookieHeaders() {
        return this.cookieHeaders;
    }

    @Override
    public void setCookieHeaders(@NotNull final List<String> cookieHeaders) {
        this.cookieHeaders = cookieHeaders;
    }

    @Override
    public void clearCookieHeaders() {
        this.cookieHeaders = Collections.emptyList();
    }

}