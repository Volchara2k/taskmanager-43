package ru.renessans.jvschool.volkov.task.manager.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class IllegalProcessCompleting extends AbstractException {

    @NotNull
    private static final String PROCESS_COMPLETING_ILLEGAL =
            "Ошибка! Нелегальный результат операции!\n";

    public IllegalProcessCompleting() {
        super(PROCESS_COMPLETING_ILLEGAL);
    }

}