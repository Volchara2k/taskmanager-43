package ru.renessans.jvschool.volkov.task.manager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICookieService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.TaskSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class TaskDeleteByIdListener extends AbstractTaskListener {

    @NotNull
    private static final String CMD_TASK_DELETE_BY_ID = "task-delete-by-id";

    @NotNull
    private static final String DESC_TASK_DELETE_BY_ID = "удалить задачу по идентификатору";

    @NotNull
    private static final String NOTIFY_TASK_DELETE_BY_ID =
            "Происходит попытка инициализации удаления задачи. \n" +
                    "Для удаления задачи по идентификатору введите идентификатор задачи из списка. ";

    public TaskDeleteByIdListener(
            @NotNull final TaskSoapEndpoint taskEndpoint,
            @NotNull final ICookieService sessionService
    ) {
        super(taskEndpoint, sessionService);
    }
    @NotNull
    @Override
    public String command() {
        return CMD_TASK_DELETE_BY_ID;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_TASK_DELETE_BY_ID;
    }

    @Async
    @Override
    @EventListener(condition = "@taskDeleteByIdListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_TASK_DELETE_BY_ID);
        @NotNull final String id = ViewUtil.getLine();
        super.sessionService.setListCookieRowRequest(super.taskEndpoint);
        final int deleteTaskResponse = super.taskEndpoint.deleteTaskById(id);
        ViewUtil.print(deleteTaskResponse);
    }

}