package ru.renessans.jvschool.volkov.task.manager.listener.project;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICookieService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.ProjectSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;

@RequiredArgsConstructor
public abstract class AbstractProjectListener extends AbstractListener {

    @NotNull
    protected final ProjectSoapEndpoint projectEndpoint;

    @NotNull
    protected final ICookieService sessionService;

}