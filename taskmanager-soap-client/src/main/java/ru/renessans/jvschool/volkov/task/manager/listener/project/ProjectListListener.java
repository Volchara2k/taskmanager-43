package ru.renessans.jvschool.volkov.task.manager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICookieService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.ProjectSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Collection;

@Component
public class ProjectListListener extends AbstractProjectListener {

    @NotNull
    private static final String CMD_PROJECT_LIST = "project-list";

    @NotNull
    private static final String DESC_PROJECT_LIST = "вывод списка проектов";

    @NotNull
    private static final String NOTIFY_PROJECT_LIST = "Текущий список проектов: ";

    public ProjectListListener(
            @NotNull final ProjectSoapEndpoint projectEndpoint,
            @NotNull final ICookieService sessionService
    ) {
        super(projectEndpoint, sessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_PROJECT_LIST;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_PROJECT_LIST;
    }

    @Async
    @Override
    @EventListener(condition = "@projectListListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_PROJECT_LIST);
        super.sessionService.setListCookieRowRequest(super.projectEndpoint);
        @Nullable final Collection<ProjectDTO> projectsResponse = super.projectEndpoint.getAllProjects();
        ViewUtil.print(projectsResponse);
    }

}