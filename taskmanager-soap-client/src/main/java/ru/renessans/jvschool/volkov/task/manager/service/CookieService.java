package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICookieRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICookieService;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidPortException;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

@Getter
@Setter
@Service
@RequiredArgsConstructor
public final class CookieService implements ICookieService {

    @NotNull
    private static final String REQUEST_HEADER = "Cookie";

    @NotNull
    private static final String RESPONSE_HEADER = "Set-Cookie";

    @NotNull
    private final ICookieRepository cookieRepository;

    @NotNull
    @Override
    public List<String> getCookieHeaders() {
        return this.cookieRepository.getCookieHeaders();
    }

    @Override
    public void clearCookieHeaders() {
        this.cookieRepository.clearCookieHeaders();
    }

    @SneakyThrows
    @Override
    @SuppressWarnings("unchecked")
    public void saveCookieHeaders(@Nullable final Object port) {
        if (Objects.isNull(port)) throw new InvalidPortException();
        @NotNull final BindingProvider bindingProvider = (BindingProvider) port;
        @NotNull final Map<String, Object> responseContext = bindingProvider.getResponseContext();
        @NotNull final Map<String, Object> httpResponseHeaders =
                (Map<String, Object>) responseContext.get(MessageContext.HTTP_RESPONSE_HEADERS);
        @NotNull final List<String> cookieHeaders = (List<String>) httpResponseHeaders.get(RESPONSE_HEADER);
        this.cookieRepository.setCookieHeaders(cookieHeaders);
    }

    @SneakyThrows
    @Override
    public void setListCookieRowRequest(@Nullable final Object port) {
        if (Objects.isNull(port)) throw new InvalidPortException();
        this.requestContext(port).put(MessageContext.HTTP_REQUEST_HEADERS, new LinkedHashMap<>());
        @NotNull final Map<String, Object> httpRequestHeaders = httpRequestHeaders(port);
        @NotNull final List<String> cookieHeaders = this.getCookieHeaders();
        httpRequestHeaders.put(REQUEST_HEADER, cookieHeaders);
    }

    @NotNull
    @SuppressWarnings("unchecked")
    private Map<String, Object> httpRequestHeaders(@NotNull final Object port) {
        @NotNull final Map<String, Object> requestContext = this.requestContext(port);
        return (Map<String, Object>) requestContext.get(MessageContext.HTTP_REQUEST_HEADERS);
    }

    @NotNull
    private Map<String, Object> requestContext(@NotNull final Object port) {
        @NotNull final BindingProvider bindingProvider = (BindingProvider) port;
        return bindingProvider.getRequestContext();
    }

}